package test.java.CRUDHolderTests;

import AdditionalPoints_sem2.CRUDHolder;
import AdditionalPoints_sem2.ForTest;
import test.java.CRUDHolderTests.CRUDHolderTest;
import org.junit.Before;
import org.junit.Test;

import java.util.NoSuchElementException;

import static org.junit.Assert.*;

public class CRUDHolderTest {
    private CRUDHolder<ForTest> crud;

    private void fillCRUDHolder(int amount) {
        for (int i = 0; i < amount; i++) {
            crud.create(new ForTest(i, ""));
        }
    }

    @Before
    public void prepare() {
        crud = new CRUDHolder<>();
        fillCRUDHolder(10);
    }

    @Test
    public void createTest() {
        int expectedSize = crud.getCount() + 1;
        crud.create(new ForTest(crud.getCount(), ""));
        assertEquals(crud.getCount(), expectedSize);

        assertThrows(
                IllegalArgumentException.class,
                () -> crud.create(new ForTest(crud.getCount() - 1, "")));
    }

    @Test
    public void readTest() {
        assertEquals(crud.read(0).getValue(), 0);

        assertThrows(
                NoSuchElementException.class,
                () -> crud.read(crud.getCount()));
    }

    @Test
    public void updateTest() {
        crud.update(new ForTest(0, "string"));
        assertEquals(crud.read(0).getString(), "string");

        assertThrows(
                NoSuchElementException.class,
                () -> crud.update(new ForTest(crud.getCount(), "")));
    }

    @Test
    public void deleteTest() {
        int expectedSize = crud.getCount() - 1;
        crud.delete(new ForTest(crud.getCount() - 1, ""));
        assertEquals(crud.getCount(), expectedSize);

        assertThrows(
                NoSuchElementException.class,
                () -> crud.delete(new ForTest(crud.getCount(), "")));
    }
}