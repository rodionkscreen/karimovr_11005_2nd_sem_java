package ControlWork02_06;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//Метод 1
//
//В файле task1.txt хранится массив покупок вида “покупатель|товар|кол-во”.
// Вывести для каждого покупателя товар с наибольшим суммарным купленным количеством

class Product{
    String product;
    String where;
    int cost;
    public Product(String product, String where, int cost) {
        this.product = product;
        this.where = where;
        this.cost = cost;
    }
}
class Purchases{
    String name;
    String where;
    String what;
    public Purchases(String name, String where, String what) {
        this.name = name;
        this.where = where;
        this.what = what;
    }
}

public class task1 {
    public static void main(String[] args){
        String path = "src/ControlWork02_06/products.txt"; //книга|IKEA|78
        Map<String, Map<String, Integer>> map = new HashMap<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(path))) {
            String s = bufferedReader.readLine();
            while (s != null) {
                String[] s1 = s.split("\\|");
                System.out.println(s1[0] + " " + s1[1] + " " + s1[2]);
                if (map.containsKey(s1[0])){
                    if (map.get(s1[0]).containsKey(s1[1])){
                        int count = map.get(s1[0]).get(s1[1]) + Integer.parseInt(s1[2]);
                        map.get(s1[0]).put(s1[1], count);
                    } else {
                        Map<String, Integer> mapTemp = new HashMap<>();
                        mapTemp.put(s1[1], Integer.parseInt(s1[2]));
                    }
                } else {
                    String a1 = s1[0]; String a2 = s1[1]; int a3 = Integer.parseInt(s1[2]);
                    Map<String, Integer> tempMap1 = new HashMap<>();
                    tempMap1.put(a2, a3);
                    Map<String, Map<String, Integer>> tempMap2 = new HashMap<>();
                    tempMap2.put(a1, tempMap1);
                }
                s = bufferedReader.readLine();
            }
            List<Product> listProducts = null;

            while(!map.isEmpty()){
                for (Map.Entry<String, Map<String, Integer>> entry : map.entrySet()) {
                    Map<String, Integer> mapTemp = entry.getValue();
                    for (Map.Entry<String, Integer> entry1 : mapTemp.entrySet()){
//                        Map<String, Integer> tempMap = entry.getValue();
                        Product product = new Product(entry.getKey(),  entry1.getKey(), entry1.getValue());
                        listProducts.add(product);
                    }
                }
            }

//        } catch (IOException e){
//            e.getMessage();
//        }
        path = "src/ControlWork02_06/purchases.txt"; //Боб|IKEA|книга
            Map<String, Map<String, String>> map2 = new HashMap<>();

        try (BufferedReader bufferedReader2 = new BufferedReader(new FileReader(path))){
                String s2 = bufferedReader2.readLine();
                while (s2 != null) {
                    String[] s1 = s2.split("\\|");
//                    System.out.println(s1[0] + " " + s1[1] + " " + s1[2]);
                    if (map2.containsKey(s1[0])){
                        if (map2.get(s1[0]).containsKey(s1[1])){
                            Map<String, String> mapTemp = new HashMap<>();
                            mapTemp.put(s1[1], s1[2]);
                            map2.put(s1[0], mapTemp);
                        } else {
                            Map<String, String> tempMap2 = new HashMap<>();
                            tempMap2.put(s1[1], s1[2]);
                            map2.put(s1[0], tempMap2);
                        }
                    } else {
                        Map<String, String> tempMap1 = new HashMap<>();
                        tempMap1.put(s1[1], s1[2]);
                        map2.put(s1[0], tempMap1);
                    }
                    s = bufferedReader.readLine();
                }
                List<Purchases> listPurchases = null;
//                while(!map2.isEmpty()){
//                    for (Map.Entry<String, Map<String, String>> entry : map.entrySet()) {
//                        Map<String, String> mapTemp = entry.getValue();
//                        for (Map.Entry<String, String> entry1 : mapTemp.entrySet()){
////                        Map<String, Integer> tempMap = entry.getValue();
//                            Purchases purchase = new Purchases(entry.getKey(),  entry1.getKey(), entry1.getValue());
//                            listPurchases.add(purchase);
//                        }
//                    }
//                }


        //TASK 2
        //Для каждого товара вывести сколько раз его купили и максимальную цену покупки. Если товар не купили ни разу, то не выводить.

    int max = -1;
    int counter = 0;
    String item = "0";
    List<Product> listProductsCopy = null;
    for (int i = 0; i < listProducts.size(); i++){
    listProductsCopy.add(listProducts.get(i));
    }
    String detect = "0";
//    while (!listProducts.isEmpty()){
    for (int i = 0; i <listProducts.size(); i++)
        if (detect == listProducts.get(i).product){
            item = listProducts.get(i).product;
            counter++;
            if (listProducts.get(i).cost > max)
                max = listProducts.get(i).cost;
        } else{
            System.out.println(item + " " + counter + " " + max);
        }


    }



            }catch (IOException e){
            e.getMessage();
        }
    }

}
//        try(Stream<String> stream = Files.lines(Paths.get("file.txt"))){
//            var t = stream.
////            Map<String, Integer> map = stream.
////                    .collect(Collectors.toMap(
////                            x -> x.name,
////                            x -> x.price));
//            Map<String, Map<String, int>> map1 = stream
//                    .collect(Collectors.groupingBy(x -> x.buyer));
//
//        }catch (IOException e){
//            e.getMessage();
//        }
