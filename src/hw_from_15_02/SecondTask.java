package hw_from_15_02;
import java.util.Iterator;
import java.util.*;

public class SecondTask {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] a = new int[4];
        for (int i : new Range(a.length)) {
            a[i] = sc.nextInt();
        }
        for (int i : new Range(a.length)) {
            System.out.println(a[i]);
        }
    }
}

class RangeInterval extends Range{
    private int start;
    private int end;

//    public RangeInterval(int start, int end){
//        this.start = start;
//        this.end = end;
//    }

    public RangeInterval(int start, int end){
        super(start);
        if (start < end){
            this.start = start;
            this.end = end;
        }
        else{
            System.out.println("Incorrect count of start/end");
        }
    }

    // set u get
    /*
    {
    public void setStart(int start){
        this.start = start;
    }
    public void setEnd(int end){
        this.end = end;
        if (start > end)
            System.out.println("Incorrect count of start/end.");
        }
    public int getStart() {
        return start;
    }
    public int getEnd() {
        return end;
    }
    }



    public Iterator<Integer> iterator(){
        return new Iterator<Integer>(){
            @Override
            public boolean hasNext(){
                return getStart() < getEnd();
            }
            @Override
            public Integer next(){
                int plus = getStart() + 1;
                setStart(getStart() + 1);
                return getStart();
            }
        };
    } */

    public Iterator<Integer> iterator(){
        return new Iterator<Integer>(){
            @Override
            public boolean hasNext(){
                return start < end;
            }
            @Override
            public Integer next(){
                return start + 1;
            }
        };
    }
}

class Range implements Iterable<Integer>{
    private final int length;
    private int counter;
    public Range(int length){
        this.length = length;
        this.counter = 0;
    }
//    public Range(){}
    public Iterator<Integer> iterator(){
        return new Iterator<Integer>(){
            @Override
            public boolean hasNext(){
                return counter < length;
            }
            @Override
            public Integer next(){
                return counter++;
            }
        };
    }
}