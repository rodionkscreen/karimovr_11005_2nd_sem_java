package hw_from_15_02;

import java.util.Iterator;

public class ThirdTask {

}

class FibonacciRange extends Range {
    private int[] arr;
    private int length;
    private int counter;
    public FibonacciRange(int length, int counter){
        super(length);
        this.counter = 0;
        this.arr = new int[]{0, 1};
    }
    public Iterator<Integer> iterator(){
        return new Iterator<Integer>(){
            @Override
            public boolean hasNext(){
                return counter < length;
            }
            @Override
            public Integer next(){
                int next = arr[0]+arr[1];
                arr[0] = arr[1];
                arr[1] = next;
                return arr[1];
            }
        };
    }
}