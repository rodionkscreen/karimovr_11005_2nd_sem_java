package hw_from_15_02;

public class FirstTask {
    public static void main(String[] args) {

    }
    public static Tuple<Integer, Integer> makeTwoValues(){
        return new Tuple<>(3, 6);
    }
}

class Tuple<A, B> {
    public final A first;
    public final B second;

    public Tuple(A first, B second) {
        this.first = first;
        this.second = second;
    }
}
class ThreeTuple<A, B, C> extends Tuple {
    public final C third;
    public ThreeTuple(A first, B second, C third){
        super(first, second);
        this.third = third;
    }
}
class FourTuple<A, B, C, D> extends ThreeTuple {
    public final D fourth;
    public FourTuple(A first, B second, C third, D fourth){
        super(first, second, third);
        this.fourth = fourth;
    }
}