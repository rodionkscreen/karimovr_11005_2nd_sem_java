//package to29_04;
//
//import java.io.*;
//import java.util.ArrayList;
//import java.util.LinkedList;
//
//class Test {
//    public static void main(String[] args) {
//        String[] names = {"Bread", "Cookie", "Butter"};
//        Storage storage = new Storage();
//        storage.createType("Milk");
//        storage.addItem("Milk");
//        storage.deleteItem(storage.getItem("milk"));
//        storage.getItem("milk");
//        for (int j = 0; j < names.length; j++)
//            for (int i = 0; i < 10; i++) {
//                storage.addItem(names[j]);
//            }
//        storage.display();
//    }
//}
//
//class Storage {
//    private final ArrayList<Type> types = new ArrayList<>();
//
//    public Storage() {
//        Type.resetId();
//    }
//
//    public void addItem(String name) {
//        for (Type t : types) {
//            // Such type already exists
//            if (t.getName().equalsIgnoreCase(name)) {
//                t.addItem();
//                Data.update(types);
//                return;
//            }
//        }
//
//        createType(name);
//        addItem(name);
//    }
//
//    public Item getItem(String name) {
//        for (Type t : types) {
//            if (t.getName().equalsIgnoreCase(name)) {
//                try {
//                    return t.getItem();
//                } catch (IllegalStateException e) {
//                    System.err.println("No such item as " + name + " in storage exception");
//                    return null;
//                }
//
//            }
//        }
//        System.err.println("No such item as " + name + " in storage exception");
//        return null;
//    }
//
//    public void deleteItem(Item item) {
//        deleteItem(item.getId());
//    }
//
//    public void deleteItem(int id) {
//        for (Type t : types) {
//            if (t.isItemFound(id)) {
//                t.deleteItem(id);
//            }
//        }
//        Data.update(types);
//    }
//
//    void createType(String name) {
//        types.add(new Type(name));
//    }
//
//    public ArrayList<Type> getTypes() {
//        return types;
//    }
//
//    public void display() {
//        System.out.println(toString());
//    }
//
//    public int getTypesCount() {
//        return types.size();
//    }
//
//    @Override
//    public String toString() {
//        return "Storage: " + types;
//    }
//
//
//}
//
//
//public class Type {
//    private final String name;
//    private int count;
//    LinkedList<Item> items = new LinkedList<>();
//
//    public Type(String name) {
//        this.name = name;
//        count = 0;
//    }
//
//    public void addItem() {
//        items.add(new Item(IdHandler.getId(), name));
//        count++;
//    }
//
//    public Item getItem() throws IllegalStateException {
//        if (!items.isEmpty()) {
//            Item item = items.get(0);
//            deleteItem(item.getId());
//            return item;
//        } else {
//            throw new IllegalStateException();
//        }
//    }
//
//    public boolean isItemFound(int id) {
//        for (Item i : items) {
//            if (i.getId() == id) {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    public void deleteItem(int id) {
//        for (int i = 0; i < items.size(); i++) {
//            if (items.get(i).getId() == id) {
//                items.remove(i);
//                count--;
//                return;
//            }
//        }
//    }
//
//    public LinkedList<Item> getItems() {
//        return items;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public static void resetId() {
//        IdHandler.updateId(0);
//    }
//
//    public int getCount() {
//        return count;
//    }
//
//    @Override
//    public String toString() {
//        return "Type {'" + name + '\'' +
//                ", items counter: " + count +
//                ", items: " + items + "}";
//    }
//
//    static class IdHandler {
//        private static final String PATH_ID = "src/Homeworks/SecondSemester/Month04/HomeworkTo29/task/data/id.txt";
//
//        static int getId() {
//            return createId();
//        }
//
//        static int createId() {
//            int id = -1;
//            try (BufferedReader reader = new BufferedReader(new FileReader(PATH_ID))) {
//                id = Integer.parseInt(reader.readLine()) + 1;
//                updateId(id);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            return id;
//        }
//
//        static void updateId(int id) {
//            try (BufferedWriter writer = new BufferedWriter(new FileWriter(PATH_ID))) {
//                writer.write(Integer.toString(id));
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//
//        private static final int ID_LENGTH = 3;
//
//        static String formatId(int id) {
//            StringBuilder formattedId = new StringBuilder();
//            for (int i = 0; i < ID_LENGTH - Integer.toString(id).length(); i++) {
//                formattedId.append(0);
//            }
//            formattedId.append(id);
//            return formattedId.toString();
//        }
//    }
//}
//
//public class StorageTest {
//    private Storage storage;
//
//    @Before
//    public void prepare() {
//        storage = new Storage();
//        storage.addItem("Bread");
//        storage.addItem("Milk");
//        storage.addItem("Grape");
//    }
//
//    @Test
//    public void addTest() {
//        storage.addItem("Bread");
//
//        assertEquals(3, storage.getTypesCount());
//        assertEquals(2, storage.getTypes().get(0).getCount());
//    }
//
//    @Test
//    public void getTest() {
//        storage.addItem("Bread");
//
//        Type itemType = storage.getTypes().get(0);
//        assertEquals("Bread", itemType.getName());
//
//        Item item = itemType.getItems().get(0);
//        assertNotNull(item);
//        assertEquals(item.getName(), itemType.getName());
//    }
//
//    @Test
//    public void failureTest() {
//        Item item = storage.getItem("&@1//#$3");
//        assertNull(item);
//    }
//
//    @Test
//    public void deleteProductTest() {
//        storage.addItem("Bread");
//        Type itemType = storage.getTypes().get(0);
//        Item item = itemType.getItems().get(0);
//        itemType.deleteItem(item.getId());
//        assertEquals(1, itemType.getCount());
//    }
//}