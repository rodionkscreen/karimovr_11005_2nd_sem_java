//package hwto01_03;
//
//import java.util.ArrayList;
//import java.util.Iterator;
//class Stack<T> {
//    private int head;
//    protected final ArrayList<T> stackArr;
//
//    public Stack() {
//        stackArr = new ArrayList<>();
//        head = -1;
//    }
//
//    public void push (T thing) {
//        stackArr.add(++head, thing);
//    }
//
//    public T pop() {
//        return stackArr.remove(head);
//    }
//
//    public T peek() { return stackArr.get(head); }
//
//    public boolean isEmpty() {
//        return (head == -1);
//    }
//
//    public Object[] toArray() {
//        return stackArr.toArray();
//    }
//
//    public void forEach() {
//        StackIterator<T> stackIterator = new StackIterator<>(this);
//        while (stackIterator.iterator().hasNext()) {
//            System.out.println(stackIterator.iterator().next() + " ");
//        }
//    }
//
//}
//
//class StackIterator<T> extends Stack<T> implements Iterable<T>  {
//    protected int counter;
//    protected final Stack<T> stack;
//    private final int length;
//
//    public StackIterator(Stack<T> stack) {
//        this.stack = stack;
//        this.length = stack.stackArr.size();
//        counter = 0;
//    }
//
//
//
//    public @NotNull Iterator<T> iterator() {
//
//        return new Iterator<>() {
//            @Override
//            public boolean hasNext() {
//
//                return counter < length;
//            }
//
//            @Override
//            public T next() {
//                return stack.stackArr.get(counter++);
//            }
//        };
//    }
//}
//
//
//
//
//public class LinkedList<T> implements Iterable<T>  {
//    protected int counter;
//    protected int length;
//    public LinkedList() {
//        length = 0;
//        counter = 0;
//        this.length = getCount();
//    }
//
//
//    @NotNull
//    @Override
//    public Iterator<T> iterator() {
//        return new Iterator<>() {
//            @Override
//            public boolean hasNext() {
//                return counter < length;
//            }
//
//            @Override
//            public T next() {
//                counter++;
//                T result = head.item;
//
//                if (head.end())
//                    head = head.next;
//                return result;
//            }
//        };
//    }
//    public void forEach() {
//        while (this.iterator().hasNext()) {
//            System.out.println(iterator().next());
//        }
//    }
//    public int getCount() {
//        return length;
//    }
//    private static class Node<T> {
//        final T item;
//        final Node<T> next;
//
//        Node() {
//            item = null;
//            next = null;
//        }
//
//        Node(T item, Node<T> next) {
//            this.item = item;
//            this.next = next;
//        }
//
//        boolean end() { return item != null || next != null; }
//    }
//
//    private Node<T> head = new Node<>();
//
//    public void push(T item) {
//        length++;
//        head = new Node<>(item, head);
//    }
//
//    public T pop() {
//        T result = head.item;
//
//        if (head.end())
//            head = head.next;
//
//        return result;
//    }
//
//    public Object peek() {
//        return head.item;
//    }
//
//}
//
//
//
//class Main {
//    public static void main(String[] args) {
//        Stack<Integer> stack = new Stack<>();
//        System.out.println("Stack");
//        stack.push(4);
//        stack.push(2);
//        stack.push(5);
//        stack.push(2);
//        stack.forEach();
//        System.out.println("Linked list: ");
//        LinkedList<Integer> linkedList = new LinkedList<>();
//        linkedList.push(4);
//        linkedList.push(11);
//        linkedList.push(3);
//        linkedList.forEach();
//
//    }
//}
//
//
