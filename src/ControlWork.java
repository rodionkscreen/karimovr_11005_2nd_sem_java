package ControlWork25_03;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;
/*
public class ControlWork {
    public static void main(String[] args) {
        OrdersHolder ordersHolder = new OrdersHolder();

    }
}


class OrdersHolder {
    ArrayList<Buyer> buyers = new ArrayList<>();
    String way = "src\\ControlWork25_03\\orders.txt";

    public OrdersHolder() {
        getBuyers();
    }
    public void getBuyers(){
        try (BufferedReader reader = new BufferedReader(new FileReader(way))) {
            String string;
            while ((string = reader.readLine()) != null) {

                String[] strings = string.split("\\|");
                addOrder(strings[0], strings[1], strings[2], Integer.parseInt(strings[3]));


            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    Buyer getBuyer(String name){
        for (int i = 0; i < buyers.size(); i++) {
            if (buyers.get(i).getName().equals(name)) {
                return buyers.get(i);
            }
        }
                buyers.add(new Buyer(name));
                return getBuyer(name);
    }
    void addOrder(String name, String city, String item, int count){
        Buyer buyer = getBuyer(name);
        buyer.addOrder(city, item, count);
    }
    public String toString(){
        return "" + buyers;
    }
    String way1 = "src\\ControlWork25_03\\sort1.txt";
    String way2 = "src\\ControlWork25_03\\sort2.txt";
    public void Sort1(){
        TreeSet<Buyer> buyers = new TreeSet<>(new BuyerByNameComparator().thenComparing(new BuyerByCityCountComparator().thenComparing(new BuyerByOrdersCountComparator())));
        buyers.addAll(this.buyers);
    }


    static class BuyerByNameComparator implements Comparator<OrdersHolder.Buyer> {
        @Override
        public int compare(OrdersHolder.Buyer o1, OrdersHolder.Buyer o2) {
            return o1.getName().compareTo(o2.getName());
        }
    }

    static class BuyerByCityCountComparator implements Comparator<OrdersHolder.Buyer> {
        @Override
        public int compare(Buyer o1, Buyer o2) {
            Integer o1Count = o1.getCitiesSize();
            Integer o2Count = o2.getCitiesSize();
            return o1Count.compareTo(o2Count);
        }
    }

    static class BuyerByOrdersCountComparator implements Comparator<OrdersHolder.Buyer> {
        @Override
        public int compare(Buyer o1, Buyer o2) {
            return Integer.compare(o1.getOrdersSize(), o2.getOrdersSize());
        }
    }
    static class Buyer {
        String name;
        ArrayList<City> cities = new ArrayList<>();

        public Buyer(String name) {
            this.name = name;
        }

        void addOrder(String cityName, String item, int count) {
            for (City city : cities) {
                if (city.getName().equals(cityName)) {
                    city.addOrder(name, count);
                    return;
                }
            }
            cities.add(new City(cityName));
            addOrder(cityName, name, count);
        }

        public String getName() {
            return name;
        }

        public int getCitiesSize() {
            return cities.size();
        }
        public int getOrdersSize(){
            int count = 0;
            for (City city : cities){
                count += city.getOrdersSize();
            }
            return count;
        }
        public String toString(){
            return "" + name + " " + cities;
        }

        static class City{
            String name;
            ArrayList<Order> orders = new ArrayList<>();
            public City(String name) {
                this.name = name;
            }
            public String getName() {
                return name;
            }
            public void addOrder(String name, int count){
                orders.add(new Order(name, count));
            }
            public int getOrdersSize(){
                return orders.size();
            }
            public String toString(){
                return "" + name + " " + orders;
            }
            static class Order{
                String name;
                int count;

                public Order(String name, int count) {
                    this.name = name;
                    this.count = count;
                }
                public String toString(){
                    return "" + name + " " + count;
                }
            }
        }
    }
}
*/