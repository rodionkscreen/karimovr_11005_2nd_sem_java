package mypractise.generics;

class Stack<T> {
    private static class Node<T>{
        T item;
        Node<T> next;
        Node(){
            item = null;
            next = null;
        }
        Node(T item, Node<T> next){
            this.item = item;
            this.next = next;
        }
        boolean end() { return item == null && next == null; }
    }

    private Node<T> head = new Node<T>();

    public void push(T item){ // Добавление нового элемента
        head = new Node<T>(item, head);
    }
    public T pop(){
        T result = head.item;
        if (!head.end())
            head = head.next;
        return result;
    }
    public T peek(){
        return head.item;
    }

    public static void main(String[] args) {
        Stack<String> stack = new Stack<>();

        for (String s : "This is String".split(" "))
            stack.push(s);
        String s;
        while ((s = stack.pop()) != null)
            System.out.println(s);
    }
}

