package mypractise.generics;
import java.util.Scanner;
import java.util.Iterator;

public class Generics2_2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] arr = new int[3];

        for (int i: new Range(arr.length)){
            arr[i]=sc.nextInt();
        }
        for (int i: new Range(arr.length)){
            System.out.println(arr[i]);
        }
    }
}


class Range implements Iterable <Integer>{
    protected final int length;
    protected int counter;
    public Range(int length){
        this.length = length;
        counter = 0;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new Iterator<Integer>() {
            @Override
            public boolean hasNext() { return counter < length; }
            @Override
            public Integer next() {
                return counter++;
            }
        };
    }
}