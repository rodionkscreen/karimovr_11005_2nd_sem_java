package mypractise.generics;

import java.util.*;

public class Generics3 {
    public static void main(String[] args) {
        // Part 1
        NotGenericClass.genericMethodPrint(5);
        NotGenericClass.genericMethodPrint("String");
        // Part 2
        Map<String, List<String>> map = newHashMap();
        List<String> list = newArrayList();
        LinkedList<Integer> linkedList = newLinkedList();

    }
    // Part 2
    public static <K, V> Map<K, V> newHashMap() { return new HashMap<K, V>(); }
    public static <T> List<T> newArrayList() { return new ArrayList<T>(); }
    public static <T> LinkedList<T> newLinkedList() { return new LinkedList<T>(); }
}
class NotGenericClass{
    // Part 1
    public static <T> void genericMethodPrint(T x){ System.out.println(x.toString()); }


}