package mypractise.generics;

public class followTheWhiteRabbit {
    public static void main(String[] args) {
        Body body = new Body();
        Head head = new Head();
        SmallHead smallHead = new SmallHead();
        Robot<SmallHead> robot = new Robot<>(body, smallHead);

        BigHead bigHead = new BigHead();
        Robot<BigHead> robot2 = new Robot<>(body, bigHead);

        robot.getHead().burn();
        robot2.getHead().turn();

//        robot = robot2;   Не можем. Логично.

        //Сырой тип:
        MediumHead mediumHead = new MediumHead();
        Robot robot3 = new Robot(body, mediumHead);
        robot3 = robot;
        robot3 = robot2;
        // В тип T из класса Robot лупит Object. Поэтому другой тип тут работает.


        Leg leg = new Leg();
//        Robot robot4 = new Robot(body, leg); // Было можно на строке перед новым классом Robot
        // Тк в классе Robot стоит просто <T> то мы можем запихнуть всё что угодно.
        // Поэтому делаем: Robot <T extends Head>
        Robot robot4 = new Robot(body,mediumHead);

        // Далее про параметризированные методы. В Robot метод foo

        Robot robot5 = new Robot(body, smallHead);
        robot5.<SmallHead, SmallHead>foo(smallHead, smallHead);


        // Ещё в методе с параметризацией может использоваться знак вопроса
        // Строки после метода foo
        Robot<SmallHead> robot6 = new Robot<>(body, smallHead);
        Robot<MediumHead> robot7 = new Robot<>(body, mediumHead);
        robot6.foo(robot7);
    }
}

//class Robot<T>{
    class Robot <T extends Head> {
    private Body body;
    private T head;

    public Robot(Body body, T head) {
        this.body = body;
        this.head = head;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public T getHead() {
        return head;
    }

    public void setHead(T head) {
        this.head = head;
    }


//    public void foo(){    Не можем(
//        head.

    public <T1, T2 extends Head> T2 foo(T1 a, T2 b) {
        return b;
    }

    public void  foo(Robot<?> obj) {
        // obj. /...

    }
}


class Body{

}

class Head{

}

class SmallHead extends Head{
public void burn(){
    System.out.println("Burn");
}
}

class MediumHead extends Head{
public void say(){
        System.out.println("Say");
    }
}

class BigHead extends Head{
public void turn(){
    System.out.println("Turn");
}
}

class Leg{

}










