package mypractise.generics;
//Док 2
public class Generics2 {
    public static void main(String[] args) {
        Holder<String> stringHolder = new Holder<>("String");
        stringHolder.item = "2 string";
//        holder.item = 2; error

//        Holder<int> intHolder = new Holder<int>(1); Нельзя! Только с ссылочными типами данных:
        Holder<Integer> integerHolder = new Holder<>(1);


        Tuple<Integer, Integer> tuple = MakeTwoValues();
        System.out.println(tuple.first + " " + tuple.second);

    }
    public static Tuple<Integer, Integer> MakeTwoValues(){
        return new Tuple<>(3, 6);
    }
}

class Holder<T> {
    public T item;

    public Holder(T item) {
        this.item = item;
    }
}

// Кортежи
class Tuple<A, B>{
    public final A first;
    public final B second;
    public Tuple(A first, B second){
        this.first = first;
        this.second = second;
    }
}
class ThreeTuple <A, B, C> extends Tuple<A, B>{
    public final C third;
    public ThreeTuple(A first, B second, C third){
        super(first, second);
        this.third = third;

    }
}