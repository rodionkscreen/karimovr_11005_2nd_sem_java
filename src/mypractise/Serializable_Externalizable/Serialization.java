package mypractise.Serializable_Externalizable;

import java.io.*;

// Сериализация - сохранение объекта в файл(в строку, поток данных)
        // Храним инфу о заказе в виде спец объекта Order1
class Order1{
    String buyer;
    String product;
    int count;

    public Order1(){}
    public Order1(String buyer, String product, int count) {
        this.buyer = buyer;
        this.product = product;
        this.count = count;
    }

    // Записываем объекты в виде строк: <buyer> <product> <count>. Для этого переопределим метод toString().
    @Override
    public String toString(){
        return buyer + " " + product + " " + count + "\n";
    }

        // Запишем в файл массив заказов (метод writeOrdersToFile(Order1[] orders)
    public static void writeOrdersToFile(Order1[] orders){
        String path = "D:\\Study\\JAVA\\2nd sem\\karimovr_11005_2nd_sem_java\\src\\mypractise\\Serializable_Externalizable\\orders.txt";
        try(FileOutputStream fileOutputStream = new FileOutputStream(path)){
            for (Order1 order : orders){
                fileOutputStream.write(order.toString().getBytes());
            }
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
    }
    // Сам: реализую чтение
    public static void printOrdersFromFile(){
        String path = "D:\\Study\\JAVA\\2nd sem\\karimovr_11005_2nd_sem_java\\src\\mypractise\\Serializable_Externalizable\\orders.txt";
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(path))){
            String s;
            while((s = bufferedReader.readLine()) != null){
                System.out.println(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class Order2{
    public String buyer;
    public BuyedProducts[] products;
    public Order2(String buyer, BuyedProducts[] products) {
        this.buyer = buyer;
        this.products = products;
    }

    public static void writeToFile(Order2[] orders){
        String path = "D:\\Study\\JAVA\\2nd sem\\karimovr_11005_2nd_sem_java\\src\\mypractise\\Serializable_Externalizable\\serialization_res2.txt";
        try(FileOutputStream fileOutputStream = new FileOutputStream(path)){
            for (Order2 order : orders){
                fileOutputStream.write(order.toString().getBytes());
            }
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
    }
    public static void readFromFile(){
        String path = "D:\\Study\\JAVA\\2nd sem\\karimovr_11005_2nd_sem_java\\src\\mypractise\\Serializable_Externalizable\\serialization_res2.txt";
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(path))){
            String s;
            while ((s = bufferedReader.readLine()) != null){
                System.out.println(s);
            }
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
    }

    public String toString(){
        String temp = "";
        for (BuyedProducts product : products){
            temp += product.toString();
        }
        return buyer + ": " + temp;
    }
}
class BuyedProducts{
    public String name;
    public int count;
    public BuyedProducts(String name, int count) {
        this.name = name;
        this.count = count;
    }

    public String toString(){
        return name + " " + count;
    }
}

public class Serialization {
    public static void main(String[] args) {
        Order1[] orders = new Order1[3];
        orders[0] = new Order1("Vova", "Chair", 2);
        orders[1] = new Order1("Visard", "Table", 1);
        orders[2] = new Order1("Adam", "Mouse", 2);
        orders[0].writeOrdersToFile(orders);
        orders[0].printOrdersFromFile();
        System.out.println("\n");

        // Усложним задачу. Теперь один заказ может содержать несколько товаров. Для этого создадим класс Order2 u BuyedProducts внутри
        Order2[] order2 = new Order2[3];
        BuyedProducts[] buyedProducts = new BuyedProducts[2];

        buyedProducts[0] = new BuyedProducts("Computer", 1);
        buyedProducts[1] = new BuyedProducts("Ferrari", 2);

        order2[0] = new Order2("Friend" , buyedProducts);
        order2[1] = new Order2("Rodion" , buyedProducts);
        order2[2] = new Order2("CoFriend" , buyedProducts);

        order2[0].writeToFile(order2);
        order2[0].readFromFile();
    }
}
