package mypractise.Serializable_Externalizable;

import java.io.*;

//Встроенная сериализация. Чтобы показать что класс сериализуемый, используется интерфейс Serializable.
// У него нет никаких методов, он просто говорит компилятору, что класс можно сериализовать.
class Order implements Serializable {
    public String buyer;
    public String product;
    public int count;

    // Для сериализации используется специальный класс - ObjectOutputStream. Соответственно, по его названию можно понять, что он записывает в выходной поток какие-то объекты. Это - надстройка над базовым потоком вывода, то бишь, для начала нужно создать какой-нибудь поток вывода, который будем передавать в поток вывода объектов. Для примера, пусть это будет поток вывода в файл:
    public void writeOrderToFile(Order order){
        String path = "D:\\Study\\JAVA\\2nd sem\\karimovr_11005_2nd_sem_java\\src\\mypractise\\Serializable_Externalizable\\serializable_res.txt";
        try(FileOutputStream fileOutputStream = new FileOutputStream(path);  ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)){
            objectOutputStream.writeObject(order);
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
    } // out.writeObject(Object o) тут -записывает состояние объекта в поток (в данном случае, поток далее записывает это состояние в файл).
}

class Main{
    public static void main(String[] args) {
        Order order = new Order(); order.buyer = "Stas"; order.product = "cookies"; order.count = 10;
        order.writeOrderToFile(order);
    }
}
//Важный момент - если класс, который мы сериализуем, имеет поля - другие классы (не примитивные типы),
// то эти классы тоже должны быть помечены Serializable:
class BuyedProduct implements Serializable{
    public String productName;
    public int buyCount;
}
class Order3 implements Serializable{
    public String buyer;
    public BuyedProduct[] products;
}

// Запрет на сериализацию поля. Для того, чтобы пометить какое-то поле несериализуемым, есть ключевое слово transient:
class User{
    String name;
    transient String password;
} // Теперь, при сериализации поле password в файл не запишется.

// Десериализация - процесс восстановления объекта из файла.
//В Java для этого есть класс ObjectInputStream - аналогично предыдущему, но на этот раз надстройка над входным потоком (или потоком ввода). Так же, как и в предыдущем примере, используем поток чтения из файла, в качестве основы для потока чтения объектов:
class Order4 implements Serializable {
    public String buyer;
    public String product;
    public int count;

    public void writeOrderToFile(Order order) {
        String path = "D:\\Study\\JAVA\\2nd sem\\karimovr_11005_2nd_sem_java\\src\\mypractise\\Serializable_Externalizable\\serializable_res.txt";
        try (FileOutputStream fileOutputStream = new FileOutputStream(path); ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
            objectOutputStream.writeObject(order);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public Order4 readOrder() {
        String path = "D:\\Study\\JAVA\\2nd sem\\karimovr_11005_2nd_sem_java\\src\\mypractise\\Serializable_Externalizable\\serializable_res.txt";
        try (FileInputStream fileInputStream = new FileInputStream(path);
             ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
            Order4 order = (Order4) objectInputStream.readObject();
            return order;
        } catch (IOException | ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
}