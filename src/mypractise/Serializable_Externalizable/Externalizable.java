package mypractise.Serializable_Externalizable;


import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

//Что, если нам нужно как-то управлять процессом сериализации? Например, зашифровать пароль перед отправкой данных на сервер, или что-то такое?
// В Java есть инструмент, позволяющий управлять процессом сериализации/десериализации объекта - интерфейс Externalizable.
// В нём есть два метода - writeExternal и readExternal, через которые, собственно, и происходит управление сериализацией:
class Order5 implements Externalizable{
    String buyer;
    String product;
    int count;

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(buyer);
        out.writeChars(product);
        out.write(count);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        buyer = (String) in.readObject();
        product = in.readLine();
        count = in.read();
//Здесь показано, что поля можно записывать и считывать по-разному, как объекты с помощью writeObject/readObject, так и напрямую, через соответствующие методы для каждого типа.
//
//Сериализация этого объекта Externalizable происходит точно так же, как и ранее - через ObjectOutputStream. Меняется только сам класс. Если добавить вывод в консоль в методы writeExternal/readExternal, то будет видно, что именно они и вызываются при сериализации.
    }
}