package mypractise.Serializable_Externalizable;

import java.io.*;

//public class MyTests implements Serializable{
//    public static void main(String[] args) {
//
//    }
//}
//
//class BuyedProd{
//    String name;
//    int count;
//    public BuyedProd(String name, int count) {
//        this.name = name;
//        this.count = count;
//    }
//}
//class Buyer{
//    String buyer;
//    BuyedProd[] buyedProds;
//
//    public Buyer(String buyer, BuyedProd[] buyedProds) {
//        this.buyer = buyer;
//        this.buyedProds = buyedProds;
//    }
//}

class Test implements Serializable{
    String produt; int count;
    public Test(String produt, int count) {
        this.produt = produt;
        this.count = count;
    }
    public String toString(){
        return produt + " " + count;
    }

    public static void main(String[] args) {
        String path = "D:\\Study\\JAVA\\2nd sem\\karimovr_11005_2nd_sem_java\\src\\mypractise\\Serializable_Externalizable\\TestOut.txt";
//        Test test = new Test("Water", 2);
//        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(path))){
//            objectOutputStream.writeObject(test);
//        } catch (IOException e){
//            e.getMessage();
//        };

        try (FileInputStream fileInputStream = new FileInputStream(path);
             ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)){
            Test test1 = (Test) objectInputStream.readObject();
            System.out.println(test1.toString());

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}