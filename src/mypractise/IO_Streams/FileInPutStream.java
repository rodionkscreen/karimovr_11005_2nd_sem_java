package mypractise.IO_Streams;

import java.io.FileInputStream;
import java.io.IOException;

public class FileInPutStream {
    public static void main(String[] args) {

        //Чтобы считать данные из файла можно воспользоваться FileInputStream
        // Все классы потоков лежат в пакете java.io
        FileInputStream fileInStream = null; // В конструктор передаём путь к файлу вместе с названием.
        try { //  Если файл лежит в корне проекта, можно указать только название
            fileInStream = new FileInputStream("D:\\Study\\JAVA\\2nd sem\\karimovr_11005_2nd_sem_java\\src\\mypractise\\IO_Streams\\exampleForInputStream.txt");

            int i = -1; // Метод read считывает один символ из потока и возвращает его в виде int.
            while ((i = fileInStream.read()) != -1){ // Если поток закончился, то -1.
                System.out.print((char) i);
            }
        }
        catch (IOException e){
            System.out.println(e.getMessage());
        }
        finally { // Если мы открыли файл, его надо закрыть, иначе он так и останется открытым, пока
            if (fileInStream != null) { // работает программа.
                try{
                    fileInStream.close();
                }
                catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            }
        }



    }
}
