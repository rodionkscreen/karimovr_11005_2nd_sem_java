package mypractise.IO_Streams;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

class FileOutputStrea {
    public static void main(String[] args) {
        // Док 23: 18.12 IO, Streams, работа с файлами
// Для записи в файл используется:
        FileOutputStream fileOutStream = null;
        String path = "D:\\Study\\JAVA\\2nd sem\\karimovr_11005_2nd_sem_java\\src\\mypractise\\IO_Streams\\exampleForOutputStream.txt";
        try {
            fileOutStream = new FileOutputStream(path);
            String text = "_hello world!";

            byte[] buffer = text.getBytes();
            fileOutStream.write(buffer);
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }
        finally {
            if (fileOutStream != null) {
                try {
                    fileOutStream.close();
                }
                catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            }
        }



        //Try-with-resources

        //Есть более простой способ контролировать закрытие файла. Это конструкция try-with-resources.
        // Выглядит она следующим образом:
        path = "D:\\Study\\JAVA\\2nd sem\\karimovr_11005_2nd_sem_java\\src\\mypractise\\IO_Streams\\exampleForOutputStream.txt";
        try (FileOutputStream fileOutStream1 = new FileOutputStream(path, true)) {
            String text = "_hello world!";

            byte[] buffer = text.getBytes();
            fileOutStream1.write(buffer);
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }

        //JavaRush info:
        /*
        // Несколько переменных одновременно. Например, копирование из одного файла в другой. Try-with-resourses позволяет
        // несколько объектов:
        path = "D:\\Study\\JAVA\\2nd sem\\karimovr_11005_2nd_sem_java\\src\\mypractise\\IO_Streams\\CopyOut.txt";
        String path2 = "D:\\Study\\JAVA\\2nd sem\\karimovr_11005_2nd_sem_java\\src\\mypractise\\IO_Streams\\CopyIn.txt";
//        try (FileInputStream fileInputStream = new FileInputStream(path);
//        FileOutputStream fileOutputStream = new FileOutputStream(path2)){
//            byte[] buffer = fileInputStream.readAllBytes();
//        }     ТАК РАБОТАЕТ С 9 ВЕРСИИ
        try (FileInputStream fileInputStream = new FileInputStream(path);
        FileOutputStream fileOutputStream = new FileOutputStream(path2)){
            ///
        } catch (IOException e) {
            e.printStackTrace();
        }
*/
        //И снова док:
        // Scanner (некст java файл)
    }
}