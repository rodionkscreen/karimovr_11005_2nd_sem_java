package mypractise.IO_Streams;

import java.io.FileOutputStream;
import java.io.IOException;

public class PrintStream {
    public static void main(String[] args) {
        String path = "D:\\Study\\JAVA\\2nd sem\\karimovr_11005_2nd_sem_java\\src\\mypractise\\IO_Streams\\exampleForInputStream.txt";
        // Класс, упрощающий ввод и вывод. Именно он используется для вывода в консоль. System.out, который
        // мы все используем - статическое поле класса System, которое имеет тип PrintStream.
        // В нём содержатся такие методы, как print, println, printf.
        // Однако, этот класс можно использовать не только для вывода в консоль, но и для вывода в любой другой поток:
     // Должно работать..
     /*   try (FileOutputStream fileOutStream = new FileOutputStream(path);
             PrintStream printStream = new PrintStream(fileOutStream)){
            String text = "Hello, world!";
            printStream.println(text);
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        } */
    }
}
