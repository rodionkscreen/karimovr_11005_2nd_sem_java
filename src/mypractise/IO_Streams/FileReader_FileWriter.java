package mypractise.IO_Streams;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileReader_FileWriter {
    public static void main(String[] args) {
        String path = "D:\\Study\\JAVA\\2nd sem\\karimovr_11005_2nd_sem_java\\src\\mypractise\\IO_Streams\\exampleForInputStream.txt";

        // С помощью всех вышеперечисленных классов можно писать в файл и читать из него,
        // однако есть классы, которые позволяют делать это удобнее.
        // Класс FileReader наследуется от Reader и позволяет читать файлы:
         try (FileReader reader = new FileReader(path)) {
            int i;
            while ((i = reader.read()) != -1) {
                System.out.print((char) i);
            }
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }


        // Аналогично FileReader, этот класс упрощает запись в файл:
        try (FileWriter writer = new FileWriter(path, true)) {
            writer.write("Hello, world");
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }
}
