package mypractise.IO_Streams;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class PrintWriter {
    public static void main(String[] args) {
        String path = "D:\\Study\\JAVA\\2nd sem\\karimovr_11005_2nd_sem_java\\src\\mypractise\\IO_Streams\\exampleForInputStream.txt";

        // Класс PrintWriter очень похож на PrintStream по поведению. Однако, как наследник класса Writer, в отличие от OutputStream,
        // он работает не с байтами, а с символами. Соответственно, при записи с помощью PrintStream, символы и строки
        // записываются в кодировке по умолчанию, тогда как PrintWriter учитывает кодировку системы.
        // Так же, в PrintWriter можно указать нужную кодировку. Использование кодировки по умолчанию может вызвать ошибки,
        // если на другом устройстве эта кодировка другая. Поэтому, для всех случаев, кроме вывода в консоль,
        // лучше использовать PrintWriter вместо PrintStream:
      /*  try (FileOutputStream fileOutStream = new FileOutputStream(path, true);
             PrintWriter writer = new PrintWriter(fileOutStream)){

            String text = "Hello, world!";
            writer.println(text);
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        } */

    }
}
