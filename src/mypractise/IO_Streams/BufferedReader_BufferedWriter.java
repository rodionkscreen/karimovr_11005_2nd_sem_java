package mypractise.IO_Streams;

import java.io.*;

public class BufferedReader_BufferedWriter {
    public static void main(String[] args) {
        String path = "D:\\Study\\JAVA\\2nd sem\\karimovr_11005_2nd_sem_java\\src\\mypractise\\IO_Streams\\exampleForInputStream.txt";

        // Аналогично BufferedInputStream, есть классы, наследники Writer,
        // которые добавляют буфер к операциям ввода-вывода, чем уменьшают количество обращений к файловой системе.
        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            String s;
            while ((s = reader.readLine()) != null) {
                System.out.println(s);
            }
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }


        // Работает аналогично FileWriter:
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(path))) {
            writer.write("Hello, world!");
        }
        catch (IOException e) {
            System.out.println(e);
        }

        //BufferedReader может применяться не только для считывания из файла, но и из любого потока ввода, например, с консоли.
        // Сделаем считывание из консоли в файл:
        try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(path))) {
            String s;
            while(!(s = bufferedReader.readLine()).equals("ESC")){
                bufferedWriter.write(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Аналогично, BufferedWriter может писать в консоль. Сделаем считывание из файла в консоль:
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(path));
             BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(System.out))) {
            String s;
            while ((s = bufferedReader.readLine()) != null){
                bufferedWriter.write(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        }


    }

