package mypractise.IO_Streams;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class BufferedStream {
    public static void main(String[] args) {
        //
        //В Java есть специальные классы, которые добавляют к стандартным потокам специальный буфер,
        // благодаря чему происходит меньше обращений к диску и повышается быстродействие.
        // Для потоков ввода есть класс BufferedInputStream. Прочитаем из файла с его помощью:
        String path = "D:\\Study\\JAVA\\2nd sem\\karimovr_11005_2nd_sem_java\\src\\mypractise\\IO_Streams\\exampleForInputStream.txt";
        try (FileInputStream fileInputStream = new FileInputStream(path);
             BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream)) {
            int i;
            while ((i = bufferedInputStream.read()) != -1) {
                System.out.println((char) i);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } //  BufferedOutputStream работает аналогично.

    }
}