package mypractise.IO_Streams;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

class Scanne {
    public static void main(String[] args) {

        // Scanner
        // Сканер, который мы использовали ранее, принимает на вход объект типа InputStream.
        // System.in - как раз такой. Но сканер можно использовать и для других потоков ввода, например, для файлов:
        String path = "D:\\Study\\JAVA\\2nd sem\\karimovr_11005_2nd_sem_java\\src\\mypractise\\IO_Streams\\exampleForInputStream.txt";
        try(FileInputStream fileInputStream = new FileInputStream(path)){
            Scanner in = new Scanner(fileInputStream);
            System.out.println(in.nextLine());
        } catch (IOException e){
            System.out.println(e.getMessage());
        }

    }
}
