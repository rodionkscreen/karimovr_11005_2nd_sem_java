package mypractise.Stream_API_1;

import java.util.*;
import java.util.stream.Collectors;

public class practise4 {
    public static void main(String[] args) {
        //Методы группировки класса Collectors
        //В классе Collectors есть несколько методов, применяемых дополнительно к Collectors.groupingBy, чтобы
        // сгруппировать элементы так, как нужно. Чтобы вспомнить, по умолчанию элементы группируются в пары
        // ключ - список элементов с этим ключом:
        Purchase[] purchases = new Purchase[] {
                new Purchase("Vova" , "Water", 2),
                new Purchase("Ruslan", "Bread", 5),
                new Purchase("Vova", "Meat", 15)
        };
        Map<String, List<Purchase>> result = Arrays.stream(purchases)
                .collect(Collectors.groupingBy(x -> x.buyer));

        //Метод counting
        //Уже был ранее, позволяет подсчитать количество элементов в группе:
        Map<String, Long> result1 = Arrays.stream(purchases)
                        .collect(Collectors.groupingBy(x -> x.buyer, Collectors.counting()));

        //Методы summingInt, summingLong, summingDouble
        //Подсчитывают сумму соответствующих типов в списках. Принимают на вход функцию преобразования объекта в списке к этому типу:
        Map<String, Integer> result2 = Arrays.stream(purchases)
                        .collect(Collectors.groupingBy(x -> x.buyer, Collectors.summingInt(x -> x.price)));

        //Методы maxBy, minBy
        //Позволяют получить максимум/минимум в этих списках. Принимают на вход компаратор:
//        Map<String, Optional<Product>> result3 = Arrays.stream(purchases)
//                        .collect(Collectors.groupingBy(x -> x.buyer,
//                                Collectors.maxBy((x, y) -> Integer.compare(x.price, y.price))));

        //Метод mapping
        //Преобразовывает объекты списка к другому типу. Принимает на вход функцию преобразования и ещё один
        // метод Collectors, что позволяет делать несколько уровней вложенности в мапе, или собрать не в список, а, например, множество:
        Map<Object, Set<Object>> result4 = Arrays.stream(purchases)
                .collect(Collectors.groupingBy(x -> x.buyer,
                        Collectors.mapping(x -> x.name, Collectors.toSet())));

//        Метод reducing
//        Аналогично методу Stream.reduce(), вычисляет над списком функцию и сохраняет её результат в мапу:
        Map<String, Integer> result5 = Arrays.stream(purchases)
                .collect(Collectors.groupingBy(x -> x.buyer,
                        Collectors.mapping(x -> x.price,
                                Collectors.reducing(0, Integer::sum))));
//        Здесь список объектов, получившийся в результате группирования, сначала превращается в список int,
//        а затем над ним выполняется reducing.
//        То же самое, без метода mapping, сразу reducing с преобразованием типа:
        Map<String, Integer> result6 = Arrays.stream(purchases)
                .collect(Collectors.groupingBy(x -> x.buyer,
                        Collectors.reducing(
                                0,
                                x -> x.price,
                                Integer::sum)));
//        Здесь появляется отличие метода Collectors.reducing от обычного Stream.reduce.
//        В перегрузке метода с преобразованием типов второй аргумент - функция преобразования из исходного типа в нужный.
//        В обычном reduce там функция от двух аргументов, здесь - от одного.

//                Метод joining
//        Есть массив продуктов, с именами “table”, “chair”, “phone”.
//        Объединяет поток строк в одну строку:

        String result7 = Arrays.stream(purchases)
                .map(x -> x.name)
                .collect(Collectors.joining());
//        Вывод:
//        tablechairphone
//        Можно указать разделитель (получится операция, противоположная split()):
        String result8 = Arrays.stream(purchases)
                .map(x -> x.name)
                .collect(Collectors.joining(" - "));


//        Вывод:
//        table - chair - phone

//        Ещё можно указать префикс и суффикс для всей получившейся строки:

        String result9 = Arrays.stream(purchases)
                .map(x -> x.name)
                .collect(Collectors.joining(" - ", "[", "]"));
//        Вывод:
//[table - chair - phone]
    }
}
