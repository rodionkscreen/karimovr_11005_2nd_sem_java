package mypractise.Stream_API_1;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class practise2 {
    public static void main(String[] args) {
//        Терминальные методы Stream API
//
//        Помимо метода forEach(), который был ранее, есть и другие терминальные операции:
//
//        Методы findFirst, findAny
//        Похожи друг на друга, findFirst возвращает первый элемент в потоке, findAny - случайный.
        Stream<String> citiesStream = Stream.of("Париж", "Лондон", "Мадрид", "Берлин", "Брюссель");
        Optional<String> first = citiesStream.findFirst();
        System.out.println(first.get());
//        System.out.println(first.stream().findAny());
        //Методы возвращают объект типа Optional<T>, который является обёрткой над результатом. Подробнее о нём - позже.

//      Методы anyMatch, allMatch, noneMatch
//      Группа терминальных операторов, которые возвращают boolean значение.
//      anyMatch(Predicate<T> predicate) возвращает true, если хотя бы один элемент потока удовлетворяет условию.
//      allMatch возвращает true, если все элементы потока удовлетворяют условию.
//      noneMatch противоположен предыдущему, возвращает true, если ни один элемент не удовлетворяет условию.
//      Например, проверим, есть ли в массиве продуктов хотя бы один элемент с ценой меньше 15:
        Product[] products = new Product[] {
                new Product("table", 20),
                new Product("chair", 10),
                new Product("phone1", 40),
                new Product("phone2", 35)
        };
        boolean result = Arrays.stream(products)
                .anyMatch(x -> x.price < 15); //true


        //Методы min, max
        //
        //Возвращают минимальное и максимальное значение потока соответственно. Принимают на вход компаратор.
        //Например, найдём продукт с максимальной ценой:
        Optional<Product> product = Arrays.stream(products)
                .max((a, b) -> Integer.compare(a.price, b.price));
        System.out.println(product.get().name); //phone1

//Класс Optional<T>
//
//Перед тем, как разобрать следующие методы, необходимо понять, что из себя представляет класс Optional.
//Что, если при вызове метода findFirst, в потоке не окажется ни одного элемента?
//Для того, чтобы не возвращать null и добавить некоторые возможности для обработки таких ситуаций и придуман класс Optional.

//Он просто содержит в себе возвращаемый элемент. Получить элемент можно с помощью метода get():
        Optional<Product> product1 = Arrays.stream(products)
                .max((a, b) -> Integer.compare(a.price, b.price));
        System.out.println(product1.get().name + "\n");
//Однако, если окажется, что возвращаемого объекта нет, метод get выбросит исключение NoSuchElementException.

// Чтобы проверить наличие объекта, есть метод isPresent(), который возвращает boolean:
        Optional<Product> product2 = Arrays.stream(products)
                .max((a, b) -> Integer.compare(a.price, b.price));
        if (product2.isPresent())
            System.out.println(product2.get().name);

//Метод ifPresent()
//Позволяет сразу записать проверку на наличие объекта и его обработку в одну строчку.
// Принимает на вход функцию, которая выполнится, если объект существует:
        product = Arrays.stream(products)
                .max((a, b) -> Integer.compare(a.price, b.price));
        product.ifPresent(x -> System.out.println(x.name));

//Метод orElse()
//Позволяет указать альтернативное значение, которое будет возвращено, если объекта нет. Если объект присутствует, он и будет возвращён:
    Product t = product.orElse(new Product("empty", 0));

//Метод orElseGet()
//Аналогичен предыдущему, но принимает на вход функцию, возвращающую новый объект:
t = product.orElseGet(() -> new Product("empty", 0));

//Метод orElseThrow()
//Выбрасывает исключение, если объекта нет. Позволяет сгенерировать своё исключение, вместо NoSuchElementException.

//Метод ifPresentOrElse()
//Аналогичен методу ifPresent, но позволяет ещё и задать действие, которое будет выполнено, если объекта нет:
        product.ifPresentOrElse(
                x -> System.out.println(x.name),
                () -> System.out.println("no such element")
        );
//Принимает на вход два лямбда-выражения.


//Продолжаем
//
//Метод reduce
//Принимает на вход функцию от двух аргументов, которую применяет на элементы потока данных и возвращает результат.
// Например, посчитаем сумму цен всех продуктов:
        Optional<Integer> sum = Arrays.stream(products)
                .map(x -> x.price)
                .reduce((x, y) -> x + y);
//Первая перегрузка метода reduce принимает на вход объект BinaryOperator<T>, в котором есть одна функция:
//T apply(T a, T b);
        System.out.println(sum + "\n"); //Как видно из сигнатуры функции, она принимает на вход два аргумента
// и возвращает результат того же типа. Поэтому, мы не могли напрямую сложить цены двух продуктов,
// ибо тогда нужно было бы вернуть новый объект Product. Чтобы не плодить сущности, сначала преобразуем поток Product
// в поток Integer, а затем выполним сумму по нему.
//Если изначально был такой массив чисел: 1, 2, 3, 4, 5
//То метод reduce((x, y) -> x + y) будет аналогичен такой записи: 1 + 2 + 3 + 4 + 5

//Метод сначала вычисляет функцию от первых двух элементов, потом от получившегося результата и третьего элемента,
// потом от нового результата и четвёртого элемента и так далее, пока не дойдёт до конца. В конце оборачивает результат в Optional
// и возвращает.
//Поэтому, необходимо, чтобы функция возвращала тот же тип, что и принимала.

//Метод reduce с identity
//Следующая перегрузка помимо функции принимает на вход и начальное значение (identity).
// Теперь, даже если в потоке не было элементов, то метод reduce всё равно может вернуть какое-то значение
// (если в потоке нет элементов, вернётся значение identity). Благодаря этому, уже нет необходимости оборачивать результат в Optional:
int sum1 = Arrays.stream(products)
                .map(x -> x.price)
                .reduce(10, (x, y) -> x + y);
        System.out.println(sum1); // Видим, что даже есть элементы, identity всё равно добавляется


//Обратно в коллекцию
//
//Иногда нужно обработать коллекцию с помощью Stream API и получить на выходе новую коллекцию, а не просто вывести все элементы.
// Для этого у потока есть специальный метод collect. Можно написать свой, однако в Java уже есть класс Collectors,
// который предоставляет функции, преобразующие поток в коллекцию.

//Метод toList
//Возвращает список объектов, которые были в потоке:
List<Product> list = Arrays.stream(products).collect(Collectors.toList());
        for (Product p : list) {
            System.out.println(p.name);
        }

//У потоков есть встроенный аналогичный метод получения списка:
list = Arrays.stream(products).toList();


//Метод toSet
//Возвращает множество объектов потока:
Set<Product> set = Arrays.stream(products).collect(Collectors.toSet());
//Встроенного аналогичного метода (как у предыдущего) нет.

//Метод toMap
//Для получения Map нужно указать пару ключ-значение. Для этого в метод toMap() нужно передать две лямбды:
// которая получает ключ, и которая получает значение. Например, преобразуем массив продуктов в мапу, где ключом
// будет название, а значением - цена:
        Map<String, Integer> map = Arrays.stream(products)
                .collect(Collectors.toMap(
                        x -> x.name,
                        x -> x.price));


//Группировка элементов
//
//Вспоминаем домашку с историей покупок. Допустим, есть такой класс, который хранит информацию о покупке:
class Purchase {
    public String buyer;
    public String name;
    public int price;

    public Purchase(String buyer, String name, int price) {
        this.buyer = buyer;
        this.name = name;
        this.price = price;
    }
}
Purchase[] purchases = new Purchase[]{
        new Purchase("Vova", "Stone", 1),
        new Purchase("Ruslan", "PC", 70),
        new Purchase("Vova", "PS", 30)
};
//Метод groupingBy
//Нам нужно сгруппировать все покупки по покупателям. Для этого у класса Collectors есть метод groupingBy:
        Map<String, List<Purchase>> map1 = Arrays.stream(purchases)
                .collect(Collectors.groupingBy(x -> x.buyer));
//Теперь в мапе для каждого покупателя хранится список его покупок. Единственный минус в том,
// что информация дублируется - в каждой покупке тоже хранится имя покупателя.

//Метод partitioningBy
//Тоже группирует элементы потока, но уже в две группы, по принципу соответствуют элементы условию или нет.
// Например, отделим друг от друга элементы, чья цена больше 20 и меньше:
        Map<Boolean, List<Purchase>> map2 = Arrays.stream(purchases)
                .collect(Collectors.partitioningBy(x -> x.price > 20));
//Теперь в мапе по ключу true будет храниться список элементов с ценой больше 20, а по ключу false - список элементов с ценой меньше 20.

//Метод counting
//Подсчитывает, сколько элементов в каждой группе. Например, посчитаем, сколько товаров купил каждый покупатель
// (сами товары не сохраняются - только количество):
        Map<String, Long> map3 = Arrays.stream(purchases)
                .collect(Collectors.groupingBy(x -> x.buyer, Collectors.counting()));
//Метод Collectors.counting() передаётся вторым аргументом в метод Collectors.groupingBy.
    }
}
