package mypractise.Stream_API_1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;
// Док 17, Stream API - 1
// Док: https://docs.google.com/document/d/1h_Wxfde33bzh6xZ3dHP2VH-TU39I_gWheEkldHwDHF4/edit
//Для начала разберём один паттерн, принцип которого необходим для понимания работы методов Stream API. Допустим, есть класс,
// в котором мы передаём информацию о пользователе (куда-нибудь в другую часть программы, или на сервер):
class User {
    public final int id;
    public final String firstName;
    public final String lastName;
    public final String email;
    public final String phone;
    public final int age;

    public User(int id, String firstName, String lastName, String email, String phone, int age) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.age = age;
    }
    //Но что, если в некоторых ситуациях нужно передать неполную информацию? Например, пользователь не указал фамилию, возраст, или почту.
    // Делать поля изменяемыми нельзя, ибо нужно гарантировать, что информация не изменится в процессе. Придётся делать
    // конструкторы под каждую комбинацию полей? Или как гарантировать, что при вызове конструктора мы не перепутаем поля
    // и не зададим email вместо фамилии (так как они одного типа - String)?
    //Паттерн Builder позволяет “собирать” объект по частям. Выглядит это так:
    //https://gitlab.com/avlitskevich/javalambdas/-/blob/master/src/builder/User.java
    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private int id;
        private String firstName;
        private String lastName;
        private String email;
        private String phone;
        private int age;

        private Builder() { }

        public Builder withId(int id) {
            this.id = id;
            return this;
        }

        public Builder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder withLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder withEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder withPhone(String phone) {
            this.phone = phone;
            return this;
        }

        public Builder withAge(int age) {
            this.age = age;
            return this;
        }

        public User build() {
            return new User(id,
                    firstName,
                    lastName,
                    email,
                    phone,
                    age);
        }
    }
}

//Создаётся вложенный класс Builder, который содержит те же поля, что и User. В самом классе User есть статический метод,
// через который мы получаем объект Builder для создания пользователя.
//В классе Builder есть несколько методов, каждый из которых задаёт какое-то одно из полей и возвращает самого себя.
// Теперь можно комбинировать разные поля пользователя, и точно быть уверенными в том, какое поле задаётся в данный момент.


public class practise1 {
    public static void main(String[] args){
        User user = User.builder().withId(1).withAge(18).withFirstName("Danya").withLastName("Maf").build();
        //Финальный метод build() завершает создание пользователя и возвращает объект User с теми полями, которые были указаны до этого.
        //В этом примере задаются не все поля класса User. Остальные поля будут проинициализированы по умолчанию. Здесь это не страшно,
        // так как и int и String имеют значения по умолчанию (0 или null), но в некоторых ситуациях, когда критично важно
        // задавать какие-то значения по умолчанию, их можно записать в конструктор класса Builder.

        //Конструктор класса Builder приватный, он получается только через метод builder() у класса User.


        //Ключевое понятие в Stream API - собственно, сам Stream, или поток данных. В качестве источника используются коллекции.
        //Для начала, нужно создать поток (stream). Это можно сделать следующими способами:
        //Из коллекции:
        List<String> list = Arrays.asList("One", "Two", "Three");
        Stream<String> stream = list.stream();
        //Из значений:
        stream = Stream.of("Three", "Two", "One");
        //Из массива:
        String[] array = {"One", "Three", "Two"};
        stream = Arrays.stream(array);
        //Из файла (каждая строка файла - один элемент):
//        Stream<String> stream2 = Files.lines(Paths.get("files.txt"));
        //Из строки(каждый символ - отдельный элемент)
        IntStream stream3 = "123".chars();
        //С помощью String.Builder():
        Stream<String> stream1 = Stream.<String>builder()
                .add("asd")
                .add("abc")
                .add("example")
                .build();

        //Stream API предоставляет методы (операции), упрощающие обработку коллекций объектов. Операции есть 2х видов: промежуточные,
        // которые возвращают изменённый поток (например, где элементы отфильтрованы по какому-то признаку), и терминальные,
        // которые возвращают конечный результат (отфильтрованную коллекцию, или какое-то число)
        // У потока можно вызывать сколько угодно промежуточных операций, и в конце одну терминальную.
        // Промежуточные операции выполняются в “ленивом” режиме - они не будут выполняться, пока не вызвать терминальный оператор.
        // Это работает аналогично Builder из примера выше: можно создать builder и вызывать у него сколько угодно настроек,
        // но только последний метод build() создаёт User’а.
        //Пример отложенного выполнения:
        int[] a = new int[] {1, 2, 3, 4};
        IntStream stream2 = Arrays.stream(a).filter(x -> x % 2 == 0);
        a[0] = 10;
        stream2.forEach(System.out::println); //Если бы элементы потока вычислялись сразу же, то на второй строчке в поток
        // записались бы значения 2 и 4. Но, так как вычисление происходит только при вызове терминального оператора,
        // изменение массива в 3й строчке повлияет на итоговый результат вывода: 10, 2, 4

        //Особенности потоков:
        //Потоки не хранят элементов - они только используют/изменяют элементы, которые хранятся в коллекции
        //Операции с потоками не изменяют источник данных - изначальная коллекция остаётся такой же, каждая
        // промежуточная операция возвращает новый объект Stream.
        //Промежуточные операции выполняются отложенно - они вычисляются при вызове терминального оператора.

        //С помощью метода Stream.forEach(), который является терминальной операцией, можно перебирать элементы:
        Stream<String> stream4 = Stream.of("Париж", "Лондон", "Мадрид","Берлин", "Брюссель");
        stream4.forEach(s -> System.out.print(s + " "));

        //Метод принимает на вход объект интерфейса Consumer<T>, который содержит только один метод void consume(T).
        // Например, для строк можно передать System.out::println:
        Stream<String> stream5 = Stream.of("Париж", "Лондон", "Мадрид","Берлин", "Брюссель");
        stream5.forEach(System.out::println);
        //Это аналогично перебору всех элементов коллекции через foreach и вызову для них этого метода:
//        Stream<String> citiesStream = Stream.of("Париж", "Лондон", "Мадрид","Берлин", "Брюссель");
//        for (String s : citiesStream) {
//            System.out.println(s);
//        }

    }
}
// ФИЛЬТРАЦИЯ
//С помощью промежуточной операции filter(Predicate<T>) можно “отсеивать” элементы коллекции. Например, есть такой класс Product:

class Product{
    public String name;
    public int price;

    public Product(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public static void main(String[] args) {
        //И массив объектов этого класса:
        Product[] products = new Product[] {
                new Product("table", 20),
                new Product("chair", 10),
                new Product("phone1", 40),
                new Product("phone2", 35)
        };
        //Выведем только те продукты, у которых цена меньше 30. Для этого используем метод filter(),
        // чтобы отфильтровать нужные элементы, а затем forEach, чтобы вывести их:
        Arrays.stream(products).filter(x -> x.price < 30).forEach(x -> System.out.println(x.name));

        //ОТОБРАЖЕНИЕ
        // С помощью промежуточной операции map(Function<T, R>) можно преобразовать элементы одного типа в элементы другого типа.
        // Например, из потока продуктов получить поток их имён и вывести:
        Arrays.stream(products)
                .filter(x -> x.price < 30)
                .map(x -> x.name)
                .forEach(System.out::println);
//Метод map возвращает поток объектов String, а не Product, поэтому мы можем передать в forEach напрямую метод println.

        //СОРТИРОВКА
        //С помощью промежуточной операции sorted() можно сортировать элементы. Однако, она работает только если класс
        // реализует интерфейс Comparable (аналогично SortedSet).
        //Например, можно отсортировать строки:
        Stream<String> citiesStream = Stream.of("Париж", "Лондон", "Мадрид","Берлин", "Брюссель");
        citiesStream.sorted().forEach(System.out::println);

        //Однако, если класс не реализует Comparable, то нужно воспользоваться перегрузкой метода sorted,
        // которая принимает на вход объект Comparator. Например, отсортируем всё тот же массив продуктов по цене:
        Arrays.stream(products)
                .sorted((a, b) -> Integer.compare(a.price, b.price))
                .forEach(x -> System.out.println(x.name));

        //Метод takeWhile
        //Выбирает из потока элементы, пока они соответствуют условию. Как только метод встречает первый элемент,
        // не соответствующий условию, он завершается.
        //ЭТОТ МЕТОД РАБОТАЕТ НАЧИНАЯ С 9-Й ВЕРСИИ ЯЗЫКА!!
                Stream<Integer> numbers = Stream.of(-3, -2, -1, 0, 1, 2, 3, -4, -5);
        numbers.takeWhile(n -> n < 0)
                .forEach(System.out::println);
        //Вывод: -3 -2 -1
        //Элементы -4 и -5 не будут выведены, так как метод takeWhile остановится на 4м элементе (0 не подходит под условие n < 0).
        // В этом его отличие от метода filter.

        System.out.println();
        //Метод dropWhile
        // Аналогичен предыдущему, но пропускает элементы, пока они удовлетворяют условию:
        Stream<Integer> numbers1 = Stream.of(-3, -2, -1, 0, 1, 2, 3, -4, -5);
        numbers1.dropWhile(n -> n < 0)
                .forEach(System.out::println);
//        Вывод: 0 1 2 3 -4 -5

        //Объединение потоков
        //Статический метод concat(Stream<T> a, Stream<T> b) может объединить два потока в один общий.
        // Объединять можно только потоки одного типа (например, String и String, Product и Product):
        Stream<String> people1 = Stream.of("Tom", "Bob", "Sam");
        Stream<String> people2 = Stream.of("Alice", "Kate", "Sam");

        Stream.concat(people1, people2).forEach(System.out::println);

        //Метод distinct
        //С помощью этого метода можно удалить повторяющиеся элементы из потока:
        Stream<String> people = Stream.of("Tom", "Bob", "Sam", "Tom", "Alice", "Kate", "Sam");
        people.distinct().forEach(System.out::println);
        //Метод работает только для тех классов, в которых переопределены методы equals и hashCode.

        //Методы skip и limit
        //Метод skip(long n) пропускает первые n элементов. Метод limit(long n) наоборот, выбирает первые n элементов потока.
        //Например, выведем элементы со второго по четвёртый. Для этого пропустим первый элемент, и возьмём следующие три:
        Stream<String> citiesStream2 = Stream.of("Париж", "Лондон", "Мадрид","Берлин", "Брюссель");
        citiesStream2
                .skip(1)
                .limit(3)
                .forEach(System.out::println);

        //Метод count
        //Возвращает количество элементов в потоке. Например, если в предыдущих примерах нужно было бы посчитать
        // количество элементов, удовлетворяющих условию, вместо вывода их в консоль, то нужно было бы использовать метод count().
        main1();
    }

    //Задача 1:
    //Отфильтровать массив продуктов, оставив в нём только те, в названии которых есть слово phone,
    // отсортировать их по цене и вывести их имена.
    public static void main1() {
        System.out.println(" \n Task 1");
        Product[] products = new Product[]{
                new Product("table", 5),
                new Product("Iphone", 20),
                new Product("bad", 25),
                new Product("phone", 15)
        };
        Arrays.stream(products).filter(x -> x.name.contains("phone")).sorted((x, y) -> Integer.compare(x.price, y.price)).forEach(x -> System.out.println(x.name));
        //Задача 2:
        //Есть массив строк. Отфильтровать строки, которые начинаются с буквы a,
        // преобразовать их в поток чисел (по длинам строк), отсортировать и вывести.
        String[] strings = new String[]{
                "abc", "brc", "apple", "june"
        };
        Arrays.stream(strings).filter(x -> x.indexOf('a') == 0).map(x -> x.length()).sorted((x, y) -> Integer.compare(x, y)).forEach(System.out::println);

    }
}

