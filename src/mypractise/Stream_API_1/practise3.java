package mypractise.Stream_API_1;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Purchase {
    public String buyer;
    public String name;
    public int price;

    public Purchase(String buyer, String name, int price) {
        this.buyer = buyer;
        this.name = name;
        this.price = price;
    }
}
public class practise3 {
    public static void main(String[] args) {

        Purchase[] purchases = new Purchase[]{
                new Purchase("Vova", "Stone", 1),
                new Purchase("Ruslan", "PC", 70),
                new Purchase("Vova", "PS", 30)
        };
        Product[] products = new Product[] {
                new Product("table", 20),
                new Product("chair", 10),
                new Product("phone1", 40),
                new Product("phone2", 35)
        };
        //Метод peek()
        //Аналогично методу forEach, выполняет функцию Consumer<T> для каждого элемента, но возвращает исходный поток,
        // с которым можно дальше работать. Поток не изменяется в результате выполнения функции:
        Map<String, List<Product>> map = Arrays.stream(products)
                .peek(System.out::println)
                .collect(Collectors.groupingBy(x -> x.name));

        //Метод flatMap()
        //Превращает каждый элемент в несколько. Например, если у нас поток из массивов, или поток из списков и нужно
        // превратить его в общий поток всех элементов. Или, если нужно продублировать каждый элемент в разных вариациях.
        //Пример: выведем цены всех продуктов со скидкой и без:
        Arrays.stream(products)
                        .flatMap(x -> Stream.of(
                                x.name + ": Цена со скидкой - " + x.price * 0.9,
                                x.name + ": Цена без скидки - " + x.price))
                        .forEach(System.out::println);
        //Функция, с которой работает метод, принимает на вход объект из потока и возвращает новый поток.

        //Метод toArray()
        //Позволяет получить массив из потока. По умолчанию возвращает массив Object[]. Однако, есть перегрузка,
        // которая принимает ссылку на конструктор класса, который содержится в потоке. Получим массив имён продуктов:
        	String[] array = Arrays.stream(products)
                        .map(x -> x.name)
                        .toArray(String[]::new);
        //Передача ссылок на конструкторы работает так же, как и ссылки на методы, только вместо названия метода нужно написать слово new.

        // Частотный словарь слов - 1
        //На вход подаётся строка. Вывести, сколько раз каждое слово встречалось в этой строке.
        String input = "This is input stream. This should be few words";
        input = input.toLowerCase(); // К нижнему регистру
        String[] array1 = input.split(" ");
        Stream<String> stream = Arrays.stream(array); // Поток из массива
        Map<String, Long> map1 = stream.collect(Collectors.groupingBy(s -> s, Collectors.counting())); // Группируем массив строк в мапу.
        // Ключами будут сами слова. Но по умолчанию в значения мапы записываются списки из элементов.
        // Нам же нужно количество каждого слова. Поэтому, воспользуемся методом counting().
        map1.forEach((key, value) -> System.out.println(key + " " + value));

        // Укороченный вариант:
        Arrays.stream(input.toLowerCase().split(" "))
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()))
                .forEach((key, value) -> System.out.println(key + " - " + value));

        // Частотный словарь слов - 2
        //То же самое, что и в предыдущей задаче, но ещё нужно вывести N наиболее часто встречающихся слов
        // (N вводится с клавиатуры, или задаётся заранее).
        //Шаг 1: Возьмём результат предыдущей задачи на 3 шаге. Шаг 2: Получим entrySet из мапы и превратим в поток.
        //Шаг 3: Отсортируем с помощью метода sorted(). Сортируем по значению (количеству элементов).
        //Вывод


        Arrays.stream(input.toLowerCase().split(" "))
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()))
                .entrySet().stream()
                .sorted((x, y) -> Long.compare(x.getValue(), y.getValue()))
                .forEach(x -> System.out.println(x.getKey() + " " + x.getValue()));

        //Два массива - 1
        //Даны два массива целых чисел A и B и два числа K1 и K2. Получить последовательность (список) чисел, в котором будут все числа из А, которые меньше K1, и все числа из В, которые больше K2. Отсортировать по убыванию.
        //Шаг 1: Получим поток из первого массива и отфильтруем по элементам меньшим K1
        //Шаг 2: Аналогично со вторым массивом
        //Шаг 3: Объединим два потока в один
        //Шаг 4: Сортируем
        //Шаг 5: Получим из этого потока список и вернём как результат работы метода
//        public static List<Integer> task3(Integer[] a, Integer[] b, int k1, int k2) {
//            return Stream.concat(
//                    Arrays.stream(a).filter(x -> x < k1),
//                    Arrays.stream(b).filter(x -> x > k2))
//                    .sorted((x, y) -> -Integer.compare(x, y))
//                    .collect(Collectors.toList());
//        }



        //Два массива - 2
        //Даны два массива строк A и B и два числа K1 и K2. Получить последовательность (список) строк, в которой будут
        // все строки из А, у которых длина больше K1, и все строки из В, у которых длина меньше K2.
        // Отсортировать в лексикографическом порядке.
        //Шаг 1: Аналогично прошлой задаче, собираем два потока из массивов строк
        //Шаг 2: Собираем в общий поток
        //Шаг 3: Сортируем
        //Шаг 4: Возвращаем список из потока

        int k1 = 5; int k2 = 7;
        String[] a = new String[]{ "Answer", "should", "be", "two"};String[] b = new String[]{"Answer","should","be","there","three"};
                Stream<String> stream1 = Arrays.stream(a)
                        .filter(s -> s.length() > k1);
                Stream<String> stream2 = Arrays.stream(b)
                        .filter(s -> s.length() < k2);
                Stream<String> stream3 = Stream.concat(stream1, stream2);
                stream3.sorted((x, y) -> -x.compareTo(y))
                        .collect(Collectors.toList());


    //Группировка по последним цифрам - 1
    //Дан массив целых чисел. Из всех чисел, оканчивающихся на одну и ту же цифру, выбрать максимальные
    //Отсортировать последовательность полученных максимальных чисел по возрастанию их последних цифр.
    //Шаг 1: Группируем по последним цифрам
    //Шаг 2: Получаем из значений (списков) максимальные элементы:
    //Шаг 3: Так как max возвращает Optional, отфильтровываем по наличию элементов и преобразуем в значения:

//
//        int[] a1 = new int[] {11, 21, 52, 12}; //21, 5
//        Arrays.stream(a1)
//                .collect(Collectors.groupingBy(x -> x % 10))
//                .values()
//                .stream()
//                .map(int -> int.stream().max(Integer::compare))
//                .filter(x -> x.isPresent())
//                .map(x -> x.get())
//      Метод task5
//

        //Группировка по последним цифрам - 2
        //
        //Дан массив целых чисел. Сгруппировать его элементы, оканчивающиеся на одну и ту же цифру.
        // Преобразовать полученную мапу в мапу, где ключом будет цифра, на которую оканчиваются числа,
        // а значением - сумма этих чисел.
        //Шаг 1: Группируем по последним цифрам:
        //Шаг 2: Получаем поток из пар ключ-значение полученной мапы:
        //Шаг 3: Сейчас в мапе ключи - последние цифры, а значения - списки из чисел, оканчивающихся на эти цифры.
        // Собираем в новую мапу, где ключами будут ключи предыдущей мапы, а значениями - суммы этих списков:

        int[] arr = new int[] {12, 22, 2, 21, 11, 1}; // 2: 12, 22, 2; 1: 21, 11, 1.
//        Arrays.stream(arr)
//                .collect(Collectors.groupingBy(x -> x % 10))
//                .entrySet().stream()
//                 .collect(Collectors.toMap(x -> x.getKey(), x -> x.getValue().stream.reduce(0, Integer::sum)));
//







    }
        public static List<String> task4 (String[] a, String[] b, int k1, int k2){
            return Stream.concat(
                    Arrays.stream(a).filter(s -> s.length() > k1),
                    Arrays.stream(b).filter(s -> s.length() < k2))
                    .sorted((x, y) -> -x.compareTo(y))
                    .collect(Collectors.toList());
    }

    public static List<Integer> task5(Integer[] a) {
        return Arrays.stream(a)
                .collect(Collectors.groupingBy(x -> x % 10))
                .values()
                .stream()
                .map(integers -> integers.stream().max(Integer::compare))
                .filter(x -> x.isPresent())
                .map(x -> x.get())
                .sorted((x, y) -> Integer.compare(x % 10, y % 10))
                .collect(Collectors.toList());
    }

//    Шаг 4: Итоговый метод:
    public static Map<Integer, Integer> task6(Integer[] a) {
        return Arrays.stream(a)
                .collect(Collectors.groupingBy(x -> x % 10))
                .entrySet().stream()
                .collect(Collectors.toMap(x -> x.getKey(),
                        x -> x.getValue().stream().reduce(0, Integer::sum)));
    }





}
