package mypractise.Lambdas;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

// Док 15, Лямбды 1
// https://docs.google.com/document/d/15xYL2PaSValz5pEThSMdIZ-yA0hMO0NV95KQ5g_z_ts/edit
//Знак -> это лямбда-оператор. Слева от него стоят передаваемые аргументы, а справа - выражение,
// которое выполняется с этими аргументами. Аргументы x и y здесь не какие-то переменные, определённые выше,
// а те аргументы, которые будут передаваться в функцию, определённую в интерфейсе.
public class Lambda1 {
    public static void main(String[] args) {
        //Выражение, стоящее справа от лямбда-оператора, не выполняется в этот же момент. Это похоже на то,
        // как работают анонимные классы: создаётся функция и сохраняется в переменную, для того, чтобы её можно было выполнять позже.
        Operation sum = (int x, int y) -> x + y;
        Operation diff = (int x, int y) -> x - y;
        int x = sum.calculate(10, 20);
        System.out.println(x);
        System.out.println(x = diff.calculate(x, 20));
    }
    //Ту же операцию суммы можно переписать вот так (через создание анонимного класса):
    //
    //Operation sum = new Operation() {
    //            @Override
    //            public int calculate(int x, int y) {
    //                return x + y;
    //            }
    //        };
}

interface Operation{
    Map<Character, Operation> operations = null;
    public int calculate(int x, int y);

//    Operation sum = (int x, int y) -> x + y;
//    Operation diff = (int x, int y) -> x - y;

    //Для блока кода ниже
    public static int calculate(int x, int y, char c){
        if (operations.containsKey(c)) {
            Operation o = operations.get(c);
            return o.calculate(x, y);
        }
        return 0;
    }
    int z = 5;

}

//Блоки кода в лямбда-выражениях
//
//Лямбда-оператор не требует явного указания оператора return в тех случаях, когда выражение справа записывается
// в одну строчку. Однако, если в функции нужно выполнить несколько действий, то можно писать как обычную функцию (только с ; в конце):
class Lambda{
    static int a = 0;
    static int b = 0;
    public static void main(String[] args) {
        Operation sum = (int x, int y) -> {
            x *= x; y *= y;
            return x + y;
        };
        int x = sum.calculate(2, 3);
        System.out.println(x);
    Map<Character, Operation> operations = new HashMap<>();
    operations.put('+', (int x1, int y) -> x1 + y);
    operations.put('-', (int x1, int y) -> x1 - y);
    operations.put('*', (int x1, int y) -> x1 * y);
    operations.put('/', (int x1, int y) -> x1 / y);
    Operation operation = null;
//    x = Operation.calculate(1, 2, '*');
//        System.out.println(x);

    //Лямбда-выражения - такие же методы, как и любые другие нестатические методы.
    // Из них можно обращаться к полям класса, в котором они были объявлены:
    Operation operation1 = (x1, y) -> {
        a = 100;
        return x1 + y + a + b;
    };
        System.out.println(operation1.calculate(1,  2));
        System.out.println(a);

//        Так же в лямбда-выражениях есть доступ к локальным переменным метода. Но его нельзя изменять внутри лямбда-выражения.
//        Это выражение может выполняться в другое время, когда сам метод уже закончился, и все его локальные переменные
//        больше не существуют. Поэтому в лямбда-выражениях можно только получать значения локальных переменных, но не изменять их.
    }
}

//Лямбды и обобщения
//Допустим, интерфейс Operation стал обобщённым:
interface Operation1<T> {
    T operation(T x, T y);
}
//    Использовать лямбда-выражения всё ещё можно, но необходимо явно типизировать объект операции:
class Lambda1_2{
    public static void main(String[] args) {

        Operation1<String> stringOperation = (x, y) -> x + y;
        Operation1<Integer> intOperation = (x, y) -> x + y;

        System.out.println(stringOperation.operation("10", "20")); //1020
        System.out.println(intOperation.operation(10, 20));//30

        Predicate<Integer> predicate = (x) -> {
            return x < 10;
        };
        System.out.println(predicate.test(5));
    }
}


//Встроенные функциональные интерфейсы
//
//В Java уже определены некоторые функциональные интерфейсы, которые можно использовать. Вот краткий список:
//
//
//Predicate<T>
//
//public interface Predicate<T> {
//    boolean test(T t);
//}
//
//
//Используется для проверки соблюдения некоторого условия. Например, чтобы пройти по массиву и отсеять элементы, которые этому условию не удовлетворяют.
//
//
//Function<T, R>
//
//public interface Function<T, R> {
//    R apply(T t);
//}
//
//
//Используется для преобразования одного объекта в другой.
//
//
//UnaryOperator<T>
//
//public interface UnaryOperator<T> {
//    T apply(T t);
//}
//
//
//Принимает на вход один параметр, выполняет над ним какие-то действия и возвращает обратно значение такого же типа (если был int, то вернёт int, и т.д.)
//
//
//BinaryOperator<T>
//
//public interface BinaryOperator<T> {
//    T apply(T t1, T t2);
//}
//
//
//Аналогичен предыдущему, только принимает на вход два параметра и так же возвращает один. То же самое, что и интерфейс Operation с начала дока, только параметризованный.
