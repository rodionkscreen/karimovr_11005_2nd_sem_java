package mypractise.Lambdas;

import java.util.*;
import java.util.function.Predicate;

import static mypractise.Lambdas.Lambda2.siftArray;

//Док 16, Лямбды 2
// https://docs.google.com/document/d/1N4vDhtty7gyR-yIsFHIcoFAjRhRBqBCORx5o_eXFx_U/edit
public class Lambda2 {
    public static void main(String[] args) {
        Operation sum = (x, y) -> x + y;
        int x = sum.calculate(10, 20);
// Переменная sum - объект, который хранит в себе только один метод - calculate. Mожно сказать, что объект sum -
// полноценный объект, а значит, его можно передавать в методы (и возвращать как результат работы методов).
        main1();
    }
    //Например, воспользуемся встроенным функциональным интерфейсом Predicate<T> и напишем метод,
    // который будет отсеивать элементы списка по условию этого предиката:
    public static List<Integer> siftArray(List<Integer> list, Predicate<Integer> predicate) {
        ArrayList<Integer> result = new ArrayList<>();
        for (int x : list) {
            if (predicate.test(x))
                result.add(x);
        }
        return result;
    }
    public static void main1() {
        //Вызывая этот метод, нужно передавать туда объект Predicate<Integer>. Можно создать класс, реализующий этот
        // интерфейс так, как нам надо, можно создать анонимный класс, а можно использовать лямбда-выражение:
        List<Integer> list = Arrays.asList(1, 2, -1, 0, 3, 4, -2, -3);
        List<Integer> greaterThanZero = siftArray(list, x -> x > 0);
        List<Integer> evenNumbers = siftArray(list, x -> x % 2 == 0);
//        Predicate<Integer> predicate = (x) -> {
//            return x < 10;
//        };

        //Лямбды как возвращаемое значение
        //
        //Точно так же можно возвращать лямбды из методов. Если метод возвращает функциональный интерфейс.
        // Например, метод, который создаёт компаратор, сравнивающий строки по их длине:
    }
    public static Comparator<String> getLengthComparator() {
        return (a, b) -> Integer.compare(a.length(), b.length());
    }

//    Ссылки на методы
//
//    Возьмём интерфейс с прошлого дока:
//    public interface Printable {
//        void print(String s);
//    }
//    И его использование:
//    Printable p = s -> System.out.println(s);
//    Здесь лямбда-выражение служит просто прослойкой для вызова какого-то существующего метода,
//    и кроме этого оно бесполезно. Было бы хорошо убрать эту прослойку и сказать тем, кто будет использовать эту функцию,
//    что мы просто подменяем её функцией println. Благо, такая возможность есть::
//    Printable p = System.out::println;
//    Так как функциональный интерфейс нужен просто для того, чтобы передать функцию куда-то в другое место,
//    то мы можем передать существующую функцию по ссылке.
}
interface Operation2{
    public int calculate(int x, int y);

}

//Оператор ::
//
//Оператор :: позволяет получить ссылку на метод, который объявлен в классе. Да, методы можно передавать как объекты.
// С помощью этого оператора мы можем передать какой-либо метод напрямую вместо написания лямбда-выражения, вызывающего этот метод.
//Например, вынесем методы, которые использовались для отсеивания элементов списка, в отдельный класс:
class Predicates {
    public static boolean isEven(int x) {
        return x % 2 == 0;
    }
    public static boolean isGreaterThanZero(int x) {
        return x > 0;
    }

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, -1, 0, 3, 4, -2, -3);
        List<Integer> greaterThanZero = siftArray(list, Predicates::isGreaterThanZero);
        List<Integer> evenNumbers = siftArray(list, Predicates::isEven);

    }
}

//Ссылки на нестатические методы
//
//Для передачи ссылки на статические методы достаточно указать имя класса. Но можно передавать и ссылки на нестатические методы. Для этого нужно указать конкретный объект, чей метод передаётся. Для примера, создадим интерфейс с методом, который принимает на вход строку, но ничего не возвращает:
interface Calculation {
    void calculate(String input);
}
//Сигнатура метода calculate совпадает с сигнатурой метода processInput в калькуляторе, поэтому мы можем создать объект калькулятора и передать его метод в этот интерфейс:
//class Lambda2_1{
//    public static void main(String[] args) {
//        Calculator calculator = new Calculator();
//        Scanner in = new Scanner(System.in);
//
//        Calculation calc = calculator::processInput;
//        calc.calculate(in.nextLine());
//
//    }
//}

//Паттерн “Наблюдатель” (Observer)
//
//Допустим, есть класс, который изображает выключатель:
//class Switcher {
//    public void switchOn() {
//        System.out.println("Выключатель включён");
//    }
//}
//И есть класс, описывающий лампу, которая включается, когда выключатель срабатывает:
//class Lamp {
//    public void lightOn() {
//        System.out.println("Лампа зажглась");
//    }
//}
//Но сейчас лампа с выключателем никак не связана. Да, мы можем добавить в выключатель поле-ссылку на лампу,
// но это не очень хорошая идея. Выключатель не должен знать ничего об объектах, которые будут включаться, потому что,
// если нужно будет изменить список этих объектов, или добавить какую-то ещё логику, то придётся изменять сам класс выключателя.
// Да и в целом, это лишняя ответственность в классе, что нарушает принцип Single Responsibility. Лучше сделать так,
// чтобы выключатель просто включался, а все зависимые от него объекты на это реагировали.
//Для того, чтобы выключатель не знал ничего об объектах, которые реагируют на него, добавим интерфейс с одним методом:
interface Electricity {
    void electricityOn();
}
//Реализуем его в классе лампы:
class Lamp implements Electricity {
    public void lightOn() {
        System.out.println("Лампа зажглась");
    }

    @Override
    public void electricityOn() {
        lightOn();
    }
}
//Создадим в выключателе список объектов типа Electricity и при выключении будем пробегаться по всем ним и вызывать метод electricityOn:
class Switcher {
    private ArrayList<Electricity> listeners = new ArrayList<>();

    public void switchOn() {
        System.out.println("Выключатель включён");

        for (Electricity listener : listeners)
            listener.electricityOn();
//Объекты, которые ожидают какого-то события и реагируют на него, называются подписчиками, или слушателями (subscribers/listeners).
//Объект, который производит событие, на которое реагируют другие, называется издателем (publisher).
//Можно сказать, что у нас есть выключатель, который может включиться, и несколько ламп (или других объектов),
// которые ждут этого события и что-то делают, когда оно произошло.
//Ещё один принцип - подписчики могут только подписаться/отписаться от события. Они не могут редактировать весь список
// подписчиков и вызывать это событие для них. Поэтому, поле listeners в выключателе приватное.
// Теперь нужно добавить два метода для подписки и отписки от события (добавление и удаление подписчика, у которого будет
// вызываться метод electricityOn):
    }
    public void addListener(Electricity listener) {
        if (!listeners.contains(listener))
            listeners.add(listener);
    }
    public void removeListener(Electricity listener) {
        if (listeners.contains(listener))
            listeners.remove(listener);
    }
    //Теперь в main мы можем создать несколько ламп и подписать их на событие включения выключателя:
        public static void main(String[] args) {
            Switcher switcher = new Switcher();

            Lamp lamp1 = new Lamp();
            Lamp lamp2 = new Lamp();

            switcher.addListener(lamp1);
            switcher.addListener(lamp2);

            switcher.switchOn();
        }
}
//И теперь можно добавлять новые объекты в подписчики:
class Radio implements Electricity {
    @Override
    public void electricityOn() {
        System.out.println("Music is playing");
    }
}
//Если в подписчиках будет какая-то сложная логика, то можно и создавать для них отдельные классы.
// Но если нужно всего лишь, чтобы при возникновении события выполнялось какое-то действие, то можно использовать лямбды:
//        switcher.addListener(() -> System.out.println("Пожар!"));
