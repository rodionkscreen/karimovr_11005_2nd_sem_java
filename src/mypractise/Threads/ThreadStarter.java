package mypractise.Threads;

//public class ThreadStarter implements Runnable {
//    Thread thread;
//    public ThreadStarter(){
//        thread = new Thread(this);
//        System.out.println("New thread created! Running: " + thread.getName());
//        thread.start();
//    }
//
//    @Override
//    public void run() {
//        try {
//            for (int i = 0; i < 5; i++) {
//                System.out.println(Thread.currentThread().getName() + " " + Thread.currentThread().getId());
//                Thread.sleep(1000);
//                }
//            } catch(InterruptedException e){
//                System.out.println("Interrupted!");
//            }
//            System.out.println(Thread.currentThread().getName() + " " + Thread.currentThread().getId() + " is finished;");
//
//    }
//    public static void main(String[] args) {
//        new ThreadStarter();
//        new ThreadStarter();
//        new ThreadStarter();
//        new ThreadStarter();
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            System.out.println("Main thread is interrupted");
//        }
//        System.out.println("Exit");
//    }
//}


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class ThreadStarter implements Runnable{
    Thread thread;
    public ThreadStarter() {
        thread = new Thread(this);
        System.out.println("New thread is created " + thread.getName());
        thread.start();
    }

    @Override
    public void run() {
        try {
            for (int i = 0; i < 5; i++) {
                System.out.println(Thread.currentThread().getName() + " " + Thread.currentThread().getId());
                Thread.sleep(1000);
            }
        } catch (InterruptedException e){
            System.out.println("Interrupted");
        }
        System.out.println(Thread.currentThread().getName() + " " + Thread.currentThread().getId() + " exiting");
    }

    public static void main(String[] args) {
        new ThreadStarter();
        new ThreadStarter();
        new ThreadStarter();
        new ThreadStarter();
        try{
            Thread.sleep(1000);
        } catch (InterruptedException e){
            System.out.println("Main thread is interrupted");
        }
        System.out.println("Exiting");
    }
}