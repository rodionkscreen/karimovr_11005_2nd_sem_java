package mypractise.Threads;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Lock {
    private final ReentrantLock bankLock = new ReentrantLock(); // Thread может заново зайти (Reetrant)
    private Condition sufficientFunds = bankLock.newCondition(); // Позволяет сигнализировать
    private int[] accounts;
    void moneyTransfer(int from, int to, int amount){
        bankLock.lock();
        try{
            accounts[from] -= amount;
            accounts[to] += amount;
        } finally {
            bankLock.unlock();
        }
    }
// Trouble 1
    void Transfer(int from, int amount){
        while (accounts[from] < amount){ // Сразу мн-во потоков могут конкурировать за этот кусок кода, а только потом происходит lock
            // wait...
        }
        bankLock.lock();
        try{
            // transfer funds...
        } finally {
            bankLock.unlock();
        } // Доступ не синхронизирован. Кто-то может уменьшать деньги до вхождения transder funds
    }
    //Trouble 2
    void Transfer3(int from, int amount){
        bankLock.lock();
        try{
            while(accounts[from] < amount){
                // wait...
            }
            //transfer funds...

        } finally {
            bankLock.unlock();
        }
    }
// Fix
    void Transfer2(int from, int amount){
        bankLock.lock();
        try{
            while (accounts[from] < amount){
                sufficientFunds.await();
            }
            accounts[from] -= amount;
            sufficientFunds.signalAll();
        } catch (InterruptedException e){
            System.out.println("Interrupt");
        } finally {
            bankLock.unlock();
        }
    } // Но теперь никто не может закинуть деньги, потому что lock
}

class LeftRigthDeadLock {
    private final Object left = new Object();
    private final Object right = new Object();

    void leftRight() {
        synchronized (left) { // Неправильный порядок
            synchronized (right) {
                System.out.println("Blabla");
            }
        }
    }

    void rightLeft() {
        synchronized (right) { // Неправильный порядок
            synchronized (left) {
                System.out.println("Haha");
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        LeftRigthDeadLock lock = new LeftRigthDeadLock();
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                lock.leftRight();
                lock.rightLeft();
            }
        });
        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                lock.rightLeft();
                lock.leftRight();
            }
        });
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
    }

    void transferMoney(Integer fromAccount, Integer toAccount, int amount) throws IllegalAccessException {
        // fix - compare ID
        synchronized (fromAccount) {
            synchronized (toAccount) {
                if (fromAccount.hashCode() < amount) {
                    throw new IllegalAccessException();
                } else {
                    toAccount.intValue();
                    fromAccount.intValue();
                }
            }
        }

    }
}