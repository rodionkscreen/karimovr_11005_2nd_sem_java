package mypractise.Threads;

import java.util.concurrent.*;

// Через унаследование от Threads
//public class Creation1 extends Thread{
//    int arg;
//    int res;
//    public Creation1(int arg){
//        this.arg = arg;
//    }
////    @SneakyThrows
//    public void run(){
//        System.out.println(Thread.currentThread().getName());
//        res = arg * arg;
//    }
//
//    public static void main(String[] args) {
//        Creation1 creation1 = new Creation1(3);
//        System.out.println(Thread.currentThread().getName());
//        creation1.start();
//    }
//}

// Через интерфейс Runnable(сразу в коде):
//class Creation2{
//    static int arg;
//    static int res;
//
//    public Creation2(int arg) {
//        this.arg = arg;
//    }
//
//    public static void main(String[] args) {
//        Runnable runnable = new Runnable() {
//            @Override
//            public void run() {
//                System.out.println(Thread.currentThread().getName());
//                res = arg * arg;
//                System.out.println(res);
//            }
//        };
//        Creation2 creation2 = new Creation2(3);
//        Thread thread = new Thread(runnable);
//        thread.start();
//    }
//}

// Имплементируя Runnable
//class Creation2_1 implements Runnable{
//    static int arg;
//    static int res;
//    @Override
//    public void run() {
//            System.out.println(Thread.currentThread().getName());
//            res = arg * arg;
//            System.out.println(res);
//    }
//
//    public Creation2_1(int arg) {
//        this.arg = arg;
//    }
//
//    public static void main(String[] args) {
//        Creation2_1 creation2_1 = new Creation2_1(3);
//        Thread thread = new Thread(creation2_1);
//        thread.start();
//    }
//
//}

//class ThreadInThread{
//    static int arg;
//    static int res;
//    public ThreadInThread(int arg){
//        this.arg = arg;
//    }
//
//    public static void main(String[] args) {
//        Thread thread = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                res = arg * arg;
//                System.out.println(Thread.currentThread().getName());
//            }
//        });
//        Thread thread1 = new Thread(thread);
//        thread1.start();
//    }
//}


//class Callable1{
//    static int arg;
//    static int res;
////    @SneakyThrows
//    public static void main(String[] args) {
//        Callable<Integer> callable = new Callable<Integer>(){
//            @Override
//            public Integer call() throws Exception{
//                return 2*2;
//            }
//        };
//        int ans = callable.call();
//    }
//} То же самое:
//class JavaIsGood {
////    @SneakyThrows
//    public static void main(String[] args) throws Exception {
//        Callable<Integer> callable = new Callable<Integer>() {
//            @Override
//            public Integer call() {
//                return 2 * 2;
//            }
//        };
//        int answer = callable.call();
//        System.out.println("Я знаю таблицу умножения! " + answer);
//        // На выводе будет 4
//    }
//}

// Через ExecutorServices:
//class CreationExectutors{
//    static int arg;
//    public static void main(String[] args) throws InterruptedException {
//        ExecutorService executorService = Executors.newFixedThreadPool(2);
//        executorService.submit(new Runnable(){
//            @Override
//            public void run(){
//                System.out.println(Thread.currentThread().getName());
//                System.out.println(arg * arg);
//            }
//        });
//        executorService.shutdown();
//        executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
//        executorService.shutdownNow();
//    }
//}

//class CreationFuture{
//    public static void main(String[] args) {
//        Future<Integer> future = new Future<Integer>() {
//            @Override
//            public boolean cancel(boolean mayInterruptIfRunning) {
//                return false;
//            }
//
//            @Override
//            public boolean isCancelled() {
//                return false;
//            }
//
//            @Override
//            public boolean isDone() {
//                return false;
//            }
//
//            @Override
//            public Integer get() throws InterruptedException, ExecutionException {
//                return null;
//            }
//
//            @Override
//            public Integer get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
//                return null;
//            }
//        };
//        ExecutorService executorService = Executors.newSingleThreadExecutor();
//        executorService.submit(); //Можем заметить, что возвращается Future
//    }
//}