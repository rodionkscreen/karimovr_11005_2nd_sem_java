package mypractise.Threads;

import java.util.concurrent.Semaphore;

public class taskPhilosophers {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(2); // Кол-во одновременных потоков
        new Thread(new Philosopher(semaphore, 1)).start();
        new Thread(new Philosopher(semaphore, 2)).start();
        new Thread(new Philosopher(semaphore, 3)).start();
        new Thread(new Philosopher(semaphore, 4)).start();
        new Thread(new Philosopher(semaphore, 5)).start();
    }
}
class Philosopher extends Thread{
    Semaphore semaphore;
    int num = 0;
    int id;
    public Philosopher(Semaphore semaphore, int id) {
        this.semaphore = semaphore;
        this.id = id;
    }
    public void run() {
        while (num < 3){
            try {
                semaphore.acquire();
            System.out.println("Philosopher " + id + " is eating");
            sleep(500);
            num++;
            System.out.println("Philosopher " + id + " ended up");
            semaphore.release();
            sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}