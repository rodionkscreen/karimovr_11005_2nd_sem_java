package mypractise.method_split;


import java.util.Arrays;

public class method_split {
    public static void main(String[] args) {
        // Сигнатура: String[] split(String regex) regex - шаблон регулярного выражения,
        // который применяется к исходной строке и по совпадениям определяет в ней символ (или комбинацию символов) разделителя.
        // Дефолтное использование:
        String string = "This is a few elements of string";
        String[] strings = string.split(" ");
        for (String str : strings){
            System.out.println(str);
        }
        {
            System.out.println("==========================="); }

        // Нюанс: если первым элементом будет разделитель regex, то первый элемент массива будет пустой.
        string = " First element is space";
        strings = string.split(" ");
        for (String str : strings)
            System.out.println(str);
        // А вот если разделитель последний элемент, то он просто не учитывается:
        string = "Last element is space ";
        strings = string.split(" ");
        for (String str : strings)
            System.out.println(str);
        System.out.println("Check");
        System.out.println();

        // В классе String есть перегруженный метод split с сигнатурой: String[] split(String regex, int limit)
        // limit определяет, какое кол-во раз будет применяться regex к строке
        // limit > 0: Длина созданного массива не будет превышать значение limit:
        print("Limit is equals 2".split(" ", 2));
        print("Limit is equals 3".split(" ", 3));
        // limit < 0: Шаблон для поиска разделителя применяется максимальное кол-во раз:
        print("Limit is -5 ".split(" ", -5));
        // Заметим, что в конце появился один пустой элемент. Это и есть отличие от следуюего
        // limit = 0: Шаблок для поиска разделителя применяется столько раз, сколько возможно:
        print("Limit is 0 ".split(" ", 0));

        // На сомом деле, реализация метода split с одним аргументом вызывает split с двумя, где второй - ноль:
        // public String[] split(String regex){
        // return split(regex, 0);


        // Пример задачи:
        String orderInfo = "1,огурцы,20.05;2,помидоры,123.45;3,зайцы,0.50";
        System.out.println(getTotalOrderAmount(orderInfo));
        // Вывод: 144.0


    }
    static void print(String[] string){
        System.out.println(Arrays.toString(string));
    }

    static double getTotalOrderAmount(String orderInfo) {
        double totalAmount = 0d;
        final String[] items = orderInfo.split(";");

        for (String item : items) {
            final String[] itemInfo = item.split(",");
            totalAmount += Double.parseDouble(itemInfo[2]);
        }

        return totalAmount;
    }
}
