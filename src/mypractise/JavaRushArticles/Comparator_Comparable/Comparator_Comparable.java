package mypractise.JavaRushArticles.Comparator_Comparable;

import java.util.Random;
import java.util.Comparator;
// https://javarush.ru/groups/posts/1939-comparator-v-java
class Message{
    private String text;
    private int id;

    public Message(String text) {
        this.text = text;
        this.id = new Random().nextInt(1000);
    }

    public String getText() {
        return text;
    }
    public Integer getId() { // INTEGER - РЕАЛИЗУЕТ comparable
        return id;
    }
    public String toString(){
        return "[" + id + "] " + text;
    }
    public int compareTo1(int o2){
        return getId() - o2;
    }
}

public class Comparator_Comparable {
    //Comparator возвращает int по следующей схеме:
    //отрицательный int (первый объект отрицательный, то есть меньше)
    //положительный int (первый объект положительный, хороший, то есть больший)
    //ноль = объекты равны
    //Hапишем компаратор. Нам потребуется импорт java.util.Comparator. После импорта добавим в main метод:
    public static void main(String[] args) {
        Message o1 = new Message("Text1");
        Message o2 = new Message("Text2");

        Comparator<Message> comparator = new Comparator<Message>(){
            public int compare(Message o1, Message o2) {
                return o1.getId().compareTo(o2.getId());
            }
        };
        Comparator<Message> comparator1 = (o11, o21) -> o1.getId().compareTo(o2.getId()); // В скобочках — то, как мы назовём переменные. Java сама увидит, что т.к. метод то всего один, то понятно какие входные параметры нужны, сколько, каких типов. Далее мы говорим стрелочкой, что хотим их передать вот в этот участок кода.
        // Или:
        Comparator<Message> comparator2 = Comparator.comparing(obj -> obj.getId());

        // Компараторы можно объединять в цепочку. Например:
        Comparator<Message> comparator3 = Comparator.comparing(obj -> obj.getId());
        comparator3 = comparator.thenComparing(obj -> obj.getText().length());

        //Конечно же, мы можем сортировать коллекции таким образом. Но не все, а только списки. И тут нет ничего необычного, т.к. именно список предполагает доступ к элементу по индексу. А это позволяет элемент номер два поменять местами с элементом номер три. Поэтому и сортировка таким образом есть только для списков:
        //Comparator<Message> comparator = Comparator.comparing(obj -> obj.getId());
        //Collections.sort(messages, comparator);
        //
        //→ Arrays.sort (java.util.Arrays)
        //Массивы так же удобно сортировать. Опять, по той же самое причине доступа к элементам по индексу.
        //→ Наследники java.util.SortedSet и java.util.SortedMap
        //Как мы помним, у Set и Map не гарантируют порядок хранения записей. НО у нас есть специальные реализации, которые гарантируют порядок.
        //И если элементы коллекции не реализуют java.lang.Comparable, то мы в конструктор таких коллекций можем передать Comparator:
        //Set<Message> msgSet = new TreeSet(comparator);


    }
}
