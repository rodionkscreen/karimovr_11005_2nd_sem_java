package mypractise.HashMap;
import java.lang.reflect.Array;
import java.util.*;

class HashMap1 {
    public static void main(String[] args) {

        HashMap<Integer, String> passAndName = new HashMap<>();
        passAndName.put(212133, "Лидия Аркадьевна Бубликова");
        passAndName.put(162348, "Иван Михайлович Серебряков");
        passAndName.put(8082771, "Дональд Джон Трамп");
        System.out.println(passAndName);

        passAndName.put(8082771, "Витя Аркадьев Мыл");
        System.out.println(passAndName);
// Получение:
        String value = passAndName.get(8082771);
        System.out.println(value);
// Удаление:
        passAndName.remove(8082771);
        System.out.println(passAndName);
// Проверка наличия ключа и значения:
        System.out.println(passAndName.containsKey(11111));
        System.out.println(passAndName.containsValue("Иван Михайлович Серебряков"));
// Получение списка всех ключей и значений
        Set<Integer> keys = passAndName.keySet();
        System.out.println("Ключи: " + keys);

        ArrayList values = new ArrayList(passAndName.values());
        System.out.println("Значения: " + values);
// Что ещё можно:
        System.out.println(passAndName.size());
        System.out.println(passAndName.isEmpty());
        passAndName.clear();
        System.out.println(passAndName);
// Объединение двух мап в одну:
        HashMap<Integer, String> map1 = new HashMap<>();
        map1.put(1, "Аркаша");
        map1.put(2, "Марин");
        HashMap<Integer, String> map2 = new HashMap<>();
        map2.put(3, "Родя");
        map2.put(4, "Никита");
        map1.putAll(map2);
        System.out.println(map1);
//1. Перебор HashMap в цикле
//  Интерфейс Map.Entry обозначает как раз пару “ключ-значение” внутри словаря.
//  Метод entrySet() возвращает список всех пар в нашей HashMap
//  (поскольку наша мапа состоит как раз из таких пар-Entry, то мы перебираем именно пары, а не отдельно ключи или значения).
        for (Map.Entry entry : passAndName.entrySet()){
            System.out.println(entry);
//            int key = (int) entry.getKey();
//            String name = (String) entry.getValue();
        }
//Так же мы можем использовать Iterator, особенно в версиях младше JDK 1.5
//        Iterator itr = Map.entrySet().iterator();
//        while(itr.hasNext()) {
//            Entry entry = itr.next();
//            //получить ключ
//            K key = entry.getKey();
//            //получить значение
//            V value = entry.getValue();
//        }

        //2. Упорядочивание Map по ключам
        // Первый способ: добавить Map.Entry в список, и упорядочить с использованием компаратора, что сортирует по значениям.
//        List list = new ArrayList(Map.entrySet());
//        Collections.sort(list, new Comparator() {
//
//            @Override
//            public int compare(Entry e1, Entry e2) {
//                return e1.getKey().compareTo(e2.getKey());
//            }
//        });
        //Другой способ: использовать SortedMap, которая ко всему, ещё и выстраивает свои ключи по порядку.
        // Но, все ключи при этом должны воплощать Comparable или приниматься компаратором.
        //Один из имплементированных классов SortedMap — TreeMap. Её конструктор принимает компаратор.
        // Следующий код показывает как превратить обычную Map в упорядоченную.
//        SortedMap sortedMap = new TreeMap(new Comparator() {
//
//            @Override
//            public int compare(K k1, K k2) {
//                return k1.compareTo(k2);
//            }
//
//        });
//        sortedMap.putAll(Map);

        //3. Упорядочивание Map по значениям
        //Добавление Map в список и последующая сортировка работают и в данном случае, но нужно в этот раз брать Entry.getValue().
        // Код ниже почти такой же как и раньше.
        //List list = new ArrayList(Map.entrySet());
        //Collections.sort(list, new Comparator() {
        //
        //  @Override
        //  public int compare(Entry e1, Entry e2) {
        //    return e1.getValue().compareTo(e2.getValue());
        //  }
        //
        //});
        // Мы всё ещё можем использовать SortedMap в данном случае, но только если значения уникальны. В таком случае, вы можете обратить пару ключ-значение в значение-ключ. Это решение обладает строгим ограничением, и не рекомендуется мною.

        //4. Инициализация статической/неизменной Map
        // Хорошим способом будет скопировать мапу в неизменяемую (immutable) Map. Такая защитная техника программирования поможет вам создать не только безопасную для использования, но и так же потокобезопасную Map.
        //Для инициализации статической/неизменной Map, мы можем использовать инициализатор static (см. ниже). Проблема данного кода в том, что не смотря на объявление Map как static final, мы всё ещё можем работать с ней после инициализации, например Test.Map.put(3,"three");. Так что это не настоящая неизменность.
        //Для создания неизменяемой Map с использованием статического инициализатора, нам нужен супер анонимный класс, который мы добавим в неизменяемую Map на последнем шаге инициализации. Пожалуйста, посмотрите на вторую часть кода. Когда будет выброшено UnsupportedOperationException, если вы запустите Test.Map.put(3,"three");.
//        class Test {
//
//            private static final Map Map;
//            static {
//                Map = new HashMap();
//                Map.put(1, "one");
//                Map.put(2, "two");
//            }
//        }
//        public class Test {
//
//            private static final Map Map;
//            static {
//                Map aMap = new HashMap();
//                aMap.put(1, "one");
//                aMap.put(2, "two");
//                Map = Collections.unmodifiableMap(aMap);
//            }
//        }

        //5. Разница между HashMap, TreeMap, и Hashtable
//                                | HashMap | HashTable | TreeMap
//                -------------------------------------------------------
//        Упорядочивание          |нет      |нет        | да
//        null в ключ-значение    | да-да   | нет-нет   | нет-да
//        синхронизировано        | нет     | да        | нет
//        производительность      | O(1)    | O(1)      | O(log n)
//        воплощение              | корзины | корзины   | красно-чёрное дерево
        //6. Map с реверсивным поиском/просмотром
        //Иногда, нам нужен набор пар ключ-ключ, что подразумевает значения так же уникальны, как и ключи (паттерн один-к-одному). Такое постоянство позволяет создать "инвертированный просмотр/поиск" по Map. То есть, мы можем найти ключ по его значению. Такая структура данных называется двунаправленная Map, которая к сожалению, не поддерживается JDK.
        //Обе Apache Common Collections и Guava предлагают воплощение двунаправленной Map, называемые BidiMap и BiMap, соответственно.
        //7. Поверхностная копия Map
        //Почти все, если не все, Map в Java содержат конструктор копирования другой Map. Но процедура копирования не синхронизирована. Что означает когда один поток копирует Map, другой может изменить её структуру. Для предотвращения внезапной рассинхронизации копирования, один из них должен использовать в таком случае Collections.synchronizedMap().
        //
        //Map copiedMap = Collections.synchronizedMap(Map);
        //8. Создание пустой Map
        //Если Map неизменна, используйте:
        //Map = Collections.emptyMap();
        //Или, используйте любое другое воплощение. Например:
        //Map = new HashMap();

    }
}
