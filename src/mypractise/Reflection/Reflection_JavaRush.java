package mypractise.Reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Reflection_JavaRush {


}
    //Рефлексия (от позднелат. reflexio — обращение назад) — это механизм исследования данных о программе во время её выполнения.
    // Рефлексия позволяет исследовать информацию о полях, методах и конструкторах классов.
    //Вот основной список того, что позволяет рефлексия:
    //Узнать/определить класс объекта;
    //Получить информацию о модификаторах класса, полях, методах, константах, конструкторах и суперклассах;
    //Выяснить, какие методы принадлежат реализуемому интерфейсу/интерфейсам;
    //Создать экземпляр класса, причем имя класса неизвестно до момента выполнения программы;
    //Получить и установить значение поля объекта по имени;
    //Вызвать метод объекта по имени.
 //Mеханизм рефлексии подразумевает работу с классами, то и у нас будет простой класс — MyClass:
class MyClass1 {
        private int number;
        private String name = "default";

        //    public MyClass1(int number, String name) {
//        this.number = number;
//        this.name = name;
//    }
        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public void setName(String name) {
            this.name = name;
        }

        private void printData() {
            System.out.println(number + name);
        }

        //Если вы внимательно просмотрели содержимое класса, то наверняка увидели отсутствие getter’a для поля name. Само
// поле name помечено модификатором доступа private, обратиться к нему вне самого класса у нас не выйдет => мы не можем получить его значение.
//Kакой-то невнимательный программист просто забыл написать getter. Самое время вспомнить о рефлексии!
        public static void main(String[] args) {
            //Попробуем добраться до private поля name класса MyClass:
            MyClass1 myClass1 = new MyClass1();
            int number = myClass1.getNumber();
            String name = null;
            System.out.println(number + name); //0null
            try {
                Field field = myClass1.getClass().getDeclaredField("name");
                field.setAccessible(true);
                name = (String) field.get(myClass1);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                e.printStackTrace();
            }
            System.out.println(number + name); // 0default
            //В java есть замечательный класс Class. Он представляет классы и интерфейсы в исполняемом приложении Java.
            //Далее, чтобы получить поля этого класса нужно вызвать метод getFields(), этот метод вернет нам все доступные поля класса.
            // Нам это не подходит, так как наше поле private, поэтому используем метод getDeclaredFields(), этот метод также возвращает
            // массив полей класса, но теперь и private и protected. В нашей ситуации мы знаем имя поля, которое нас интересует,
            // и можем использовать метод getDeclaredField(String), где String — имя нужного поля.
//Примечание: getFields() и getDeclaredFields() не возвращают поля класса-родителя!
            //Отлично, мы получили объект Field с ссылкой на наш name. Т.к. поле не было публичным (public) следует дать доступ
            // для работы с ним. Метод setAccessible(true) разрешает нам дальнейшую работу. Теперь поле name полностью под нашим контролем!
            // Получить его значение можно вызовом get(Object) у объекта Field, где Object — экземпляр нашего класса MyClass.
            // Приводим к типу String и присваиваем нашей переменной name.

            //На тот случай если у нас вдруг не оказалось setter’a, для установки нового значения полю name можно использовать метод set:
            //field.set(myClass1, "new name");
            main1();
        }

        //Метод printData находится под модификатором доступа private, и нам пришлось самим каждый раз писать код вывода. Не порядок, где там наша рефлексия?…
        public void printData1(Object myClass1) {
            try {
                Method method = myClass1.getClass().getDeclaredMethod("printData");
                method.setAccessible(true);
                method.invoke(myClass1);
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
            //Здесь примерно такая же процедура как и с получением поля — получаем нужный метод по имени и даем доступ к нему.
            // И для вызова объекта Method используем invoke(Оbject, Args), где Оbject — все также экземпляр класса MyClass.
            // Args — аргументы метода — наш таковых не имеет.
        }

        public static void main1() {
            MyClass1 myClass1 = new MyClass1();
            int number = myClass1.getNumber();
            String name = null;
            try {
                Field field = myClass1.getClass().getDeclaredField("name");
                field.setAccessible(true);
                name = (String) field.get(myClass1);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                e.printStackTrace();
            }
            System.out.println("Test printData1: ");
            myClass1.printData1(myClass1); //0default

            //Из определения в начале ясно, что рефлексия позволяет создавать экземпляры класса в режиме runtime
            // (во время выполнения программы)! Мы можем создать объект класса по полному имени этого класса.
            // Полное имя класса — это имя класса, учитывая путь к нему в package.

            // Узнать имя класса можно простым способом (вернет имя класса в виде строки):
            MyClass1.class.getName();
            //Создадим экземпляр класса с помощью рефлексии:
            myClass1 = null;
            try {
                Class class1 = Class.forName(MyClass1.class.getName());
                myClass1 = (MyClass1) class1.newInstance();
            } catch (IllegalAccessException | ClassNotFoundException | InstantiationException e) {
                e.printStackTrace();
            }
            //На момент старта java приложения далеко не все классы оказываются загруженными в JVM. Если в вашем коде нет
            // обращения к классу MyClass, то тот, кто отвечает за загрузку классов в JVM, а им является ClassLoader,
            // никогда его туда и не загрузит. Поэтому нужно заставить ClassLoader загрузить его и получить описание нашего
            // класса в виде переменной типа Class. Для этой задачи существует метод forName(String), где String — имя класса,
            // описание которого нам требуется.
            //Получив Сlass, вызов метода newInstance() вернет Object, который будет создан по тому самому описанию.
            // Остается привести этот объект к нашему классу MyClass.

            main2();
            // Раскоментируем конкстуктор. (напишу новый класс чтобы не ломать код до этого)
        }

        class MyClass2 {
            private int number;
            private String name = "default";

            public MyClass2(int number, String name) {
                this.number = number;
                this.name = name;
            }

            public int getNumber() {
                return number;
            }

            public void setNumber(int number) {
                this.number = number;
            }

            public void setName(String name) {
                this.name = name;
            }

            private void printData() {
                System.out.println(number + name);
            }
        }

        public static void main2() {
//            MyClass2 myClass2 = null;
//            try {
//                Class clazz = Class.forName(MyClass2.class.getName());
//                Class[] params = {int.class, String.class};
//                myClass2 = (MyClass2) clazz.getConstructor(params).newInstance(1, "deafault2");
//            } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
//                e.printStackTrace();
//            }
//            System.out.println(myClass2);
            MyClass2 myClass = null;
            try {
                Class clazz = Class.forName(MyClass2.class.getName());
                Class[] params = {int.class, String.class};
                myClass = (MyClass2) clazz.getConstructor(params).newInstance(1, "default2");
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                e.printStackTrace();
            }
            System.out.println(myClass);//output created object reflection.MyClass@60e53b93
            //Для получения конструкторов класса следует у описания класса вызвать метод getConstructors(),
            // а для получения параметров конструктора - getParameterTypes():
//            Constructor[] constructors = clazz.getConstructors();
//            for (Constructor constructor : constructors) {
//                Class[] paramTypes = constructor.getParameterTypes();
//                for (Class paramType : paramTypes) {
//                    System.out.print(paramType.getName() + " ");
//                }
//                System.out.println();
            }
        }
