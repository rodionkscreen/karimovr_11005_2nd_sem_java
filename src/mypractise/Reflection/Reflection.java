package mypractise.Reflection;
// 11 doc IIsem https://docs.google.com/document/d/1BfIFZsdtFyIMuJ_RJ_imwHIyK80i1keO8qmEdXxNHpY/edit

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

// Рефлексия (отражение, интроспекция, взгляд на себя со стороны) - механизм,
// позволяющий обрабатывать данные о коде программы во время её выполнения. Краткий список  возможностей:
// 1. Узнать/определить класс объекта
// 2. Получить информацию о модификаторах класса (public/private, static, final)
// 3. Получить информацию о полях и методах класса, константах, конструкторах, суперклассах
// 4. Создать экземпляр класса, который будет известен только во время выполнения
// 5. Получить и установить значение поля объекта (любого, в том числе приватного)
// 6. Вызвать метод объекта (любой, в том числе приватный)
//
// Напишем класс, с которым будем работать
class MyClass{
    public int number;
    private String name;
    public String getName(){return name;}
    public void setNumber(int number){this.number = number;}
    private void printData(){System.out.println((number + ": " + name));}

    public MyClass(int number, String name) {
        this.number = number;
        this.name = name;
    }
}

public class Reflection {
    public static void main(String[] args) {
        // Для получения информации о классе в Java есть специальный класс, который называется Class:
        MyClass myObject = new MyClass(1, "default");
        Class myClass = myObject.getClass();
        // Выведем название класса:
        System.out.println(myClass.getName()); // getName возвращает имя класса вместе с пакетами
        System.out.println(myClass.getSimpleName()); // getSimpleName - имя класса

        System.out.println();

        // Создадим ещё один объект MyClass, только другим способом:
        try{
            MyClass myObject2 = (MyClass) myClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        // newInstance может выбросить исключение
        // myObject2 точно такой же, как и другие возможные объекты MyClass

        System.out.println();

        // Получим информацию о суперклассе:
        Class superClass = myClass.getSuperclass();
        System.out.println(superClass.getName());

        System.out.println();

        // Получение информации о полях класса. За это отвечает класс Field:
        for (Field f : myClass.getFields()){
            System.out.println(f.getName()); // number
        }
        // getFields возвращает только с модификатором default или public.
        // Чтобы получить private/protected, нужно исп-ть getDeclaredFields():
        for (Field f : myClass.getDeclaredFields()){
            System.out.println(f.getName()); // number \n name
        }
        // Получим значения приватного поля:
        try {
            Field f = myClass.getDeclaredField("name"); // getDeclaredField, тк поле приватное
            f.setAccessible(true); // иначе программа выбросит исключение IllegalAccessException
            String name = (String) f.get(myObject); // get требует передачи конкретного объекта, значение поля у которого и будет возвращаться.
            System.out.println(name);               // Мы не можем узнать значение поля без объекта, у которого есть это поле(тк оно не static)
            // Зададим новое значение приватного поля:
            f.set(myObject, "new name"); // Опять необходимо указать объект, у которого задаём новое значение поля
        } catch (NoSuchFieldException | IllegalAccessException e){
            e.printStackTrace();
        }

        System.out.println();
        System.out.println();

        // Получение информации о методах
        // Также, как и с полями, можно узнать какие методы есть в классе
        for(Method m : myClass.getDeclaredMethods()){
            System.out.println(m.getName());
        }
        // Вызовем приватный метод:
        try {
            Method m = myClass.getDeclaredMethod("printData");
            m.invoke(myObject);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e){
            e.printStackTrace();
        } // Точно так же, как и с полями, метод требует конкретный объект, у которого он будет вызываться.

        // Метод с аргументами
        // Чтобы получить метод, принимающий на вход один или несколько аргументов, нужно указать их типы:
        try {
            Method m = myClass.getDeclaredMethod("setNumber", int.class);
            m.invoke(myObject, 10);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e){
            e.printStackTrace();
        }
    }
}
