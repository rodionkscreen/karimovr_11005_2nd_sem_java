package mypractise.Annotation;

import AdditionalPoints.Meeting;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Bot {
    public String processUserInput(String input) {
        if (input.isEmpty())
            return "Я вас не понимаю";
//
//        String[] splitted = input.split(" ");
//        String command = splitted[0].toLowerCase();
//        String[] args = Arrays.copyOfRange(splitted, 1, splitted.length);
//        // Класс бота, который содержит только один метод - ответить на сообщение пользователя.
//        return "Вы ввели команду " + command;

        String[] splitted = input.split(" ");
        String command = splitted[0].toLowerCase();
        String[] args = Arrays.copyOfRange(splitted, 1, splitted.length);

        Method m = commands.get(command);

        if (m == null)
            return "Я вас не понимаю";

        try {
            return (String) m.invoke(this, (Object) args);
        } catch (Exception e) {
            return "Что-то пошло не так, попробуйте ещё раз";
        }
    }

    @AnnotationsForBot(aliases = {"привет", "hello", "hi", "q", "ку"},
            args = "",
            description = "Будь культурным, поздоровайся.")
    public String hello() {
        return "Привет!!";
    }

    @AnnotationsForBot(aliases = {"пока", "bye", "бб"},
            args = "",
            description = "")
    public String bye() {
        return "Возвращайся!";
    }

    @AnnotationsForBot(aliases = {"помощь", "команды", "help", "menu", "меню"},
            args = "",
            description = "Выводит список команд")
    public String help(String[] args) {
        StringBuilder builder = new StringBuilder("Я умею такие команды:\n");

        for (Method m : this.getClass().getDeclaredMethods()) {
            if (!m.isAnnotationPresent(AnnotationsForBot.class))
                continue;

            AnnotationsForBot cmd = m.getAnnotation(AnnotationsForBot.class);
            builder.append(Arrays.toString(cmd.aliases())).append(": ").append(cmd.description()).append(" - ").append(cmd.args()).append("\n");
        }

        return builder.toString();
    }



//Теперь можно все созданные команды объединить в один HashMap, где ключами будут названия команд, а значениями - сами методы:

    private HashMap<String, Method> commands;
    public Bot() {
        commands = new HashMap<>();
        for (Method m : this.getClass().getDeclaredMethods()) {
            if (!m.isAnnotationPresent(AnnotationsForBot.class))
                continue;
            AnnotationsForBot cmd = m.getAnnotation(AnnotationsForBot.class);
            for (String name : cmd.aliases())
                commands.put(name, m);
        }
    }
}

class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Bot bot = new Bot();

        while (true) {
            String message = in.nextLine();
            if (message.equals("exit"))
                break;

            String answer = bot.processUserInput(message); // Для закомментированного кода в боте: просто чтение с консоли и вывод в неё же.
            System.out.println(answer);
        }
    }
}
