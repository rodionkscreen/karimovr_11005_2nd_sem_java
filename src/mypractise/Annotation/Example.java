package mypractise.Annotation;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@MyAnnotation
public class Example {
    @MyAnnotation
    int a;
    @MyAnnotation
    void Method(){}

    @MyAnnotation1
    void Mehod(){}
//    @MyAnnotation1 - Уже нельзя, тк ограничили на методы.
//    int a1;
}

class PasswordUtils{
    @UseCase(id = 1, description = "Password must contains at least 1 numeric")
    public static boolean validatePassword(String password){
        return password.matches("\\w*\\d\\w*");
    }
    @UseCase(id = 2)
    public static String encruptPassword(String password){
        return new StringBuilder(password).reverse().toString();
    }
}

// Класс, который с помощью рефлексии проверит наличие нужных сценариев использования:
class UseCaseChecker{
    public static void checkUseCases(List<Integer> useCases, Class targetClass){
        for (Method m : targetClass.getDeclaredMethods()) {
            UseCase useCase = m.getAnnotation(UseCase.class);
            if (useCase == null)
                continue;
            System.out.println("Found use case: " + useCase.id() + ": " + useCase.description());
            useCases.remove((Integer) useCase.id());
        }
        for (int i : useCases){
            System.out.println("Use case " + i + " not implemented");
        }
    }

    public static void main(String[] args) {
        ArrayList<Integer> useCases = new ArrayList(Arrays.asList(1, 2, 3, 4));
        checkUseCases(useCases, PasswordUtils.class);
    }
}


class PasswordUtils1 {
    @UseCase1(value = 1, description = "Password must contains at least one numeric")
    public static boolean validatePassword(String password) {
        return password.matches("\\w*\\d\\w*");
    }

    @UseCase1(2)
    public static String encryptPassword(String password) {
        return new StringBuilder(password).reverse().toString();
    }
}

