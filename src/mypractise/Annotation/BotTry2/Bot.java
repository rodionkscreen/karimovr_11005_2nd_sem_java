package mypractise.Annotation.BotTry2;


import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Bot {
    @Command(aliases = {"hello", "привет"}, args = "", description = "Поздоровайтесь")
    public String hello(String[] args){
        return "Hello!";
    }
    @Command(aliases = {"bye", "пока"}, args = "", description = "Прощаемся")
    public String bye(String[] args){
        return "Пока!";
    }
    @Command(aliases = {"помощь", "команды", "help"},
            args = "",
            description = "Выводит список команд")
    public String help(String[] args) {
        StringBuilder builder = new StringBuilder("Я умею в такие команды:\n");

        for (Method m : this.getClass().getDeclaredMethods()) {
            if (!m.isAnnotationPresent(Command.class))
                continue;

            Command cmd = m.getAnnotation(Command.class);
            builder.append(Arrays.toString(cmd.aliases())).append(": ").append(cmd.description()).append(" - ")
                    .append(cmd.args()).append("\n");
        }

        return builder.toString();
    }


    private HashMap<String, Method> commands;

    public Bot() {
        commands = new HashMap<>();

        for (Method m : this.getClass().getDeclaredMethods()) {
            if (!m.isAnnotationPresent(Command.class))
                continue;

            Command cmd = m.getAnnotation(Command.class);
            for (String name : cmd.aliases())
                commands.put(name, m);
        }
    }

    public String processUserInput(String input) {
        if (input.isEmpty())
            return "Я вас не понимаю";

        String[] splitted = input.split(" ");
        String command = splitted[0].toLowerCase();
        String[] args = Arrays.copyOfRange(splitted, 1, splitted.length);

        Method m = commands.get(command);

        if (m == null)
            return "Я вас не понимаю";

        try {
            return (String) m.invoke(this, (Object) args);
        } catch (Exception e) {
            return "Что-то пошло не так, попробуйте ещё раз";
        }
    }
}


class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Bot bot = new Bot();

        while (true) {
            String message = in.nextLine();
            if (message.equals("exit"))
                break;

            String answer = bot.processUserInput(message);
            System.out.println(answer);
        }
    }
}


