package mypractise.Annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

public @interface MyAnnotation {} // не содержит ничего. Может применяться к чему угодно: методам, полям и классам

// Сделаем аннотицию, с ограничением по методам:
@Target(ElementType.METHOD)
@interface MyAnnotation1{}

// Можно передать сразу несколько типов:
@Target({ElementType.METHOD, ElementType.FIELD})
@interface MyAnnotation2{}

//Однако, у аннотации могут быть и параметры.
// Допустим, у проекта есть несколько сценариев использования - методов, которые должны быть реализованы.
// Тогда, для того, чтобы можно было легко отслеживать, какие сценарии реализованы, а какие нет, напишем такую аннотацию:
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME) //Определяет, доступны ли аннотации в исходном коде, в файлах классов,
// или во время выполнения (что нам и нужно).
@interface UseCase{
    public int id();
    public String description() default "No description.";
}

//У полей аннотаций могут быть значения по умолчанию - тогда их не обязательно указывать при использовании.
//Заметьте, что поля аннотаций указываются с круглыми скобками - так как аннотация это интерфейс, а у интерфейса не может быть полей.
//Сделаем класс, который будет содержать два реализованных сценария использования (далее в Example.java).

// Если одно из полей аннотации имеет название value, и других таких полей нет,
// то можно задавать его значение без указания ключа-названия поля:

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@interface UseCase1 {
    public int value();
    public String description() default "no description";
}

