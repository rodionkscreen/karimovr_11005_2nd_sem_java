package mypractise.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface AnnotationsForBot {
    String[] aliases(); // Возможные называния команды
    String args(); // Описание аргументов команды
    String description(); // Описание самой команды
}

