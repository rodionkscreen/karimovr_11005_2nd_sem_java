package hwto10_03;

public class task1 {
}

class Stack<T> {
    private static class Node<T> {
        T item;
        Node<T> next;

        Node() {
            item = null;
            next = null;
        }

        Node(T item, Node<T> next) {
            this.item = item;
            this.next = next;
        }

        boolean end() {
            return item == null && next == null;
        }
    }

    private Node<T> head = new Node<T>();

    public void push(T item) { // Добавление нового элемента
        head = new Node<T>(item, head);
    }

    public T pop() {
        T result = head.item;
        if (!head.end())
            head = head.next;
        return result;
    }

    public T peek() {
        return head.item;
    }

    public void delete() {
        head = head.next;
    }


    public static void main(String[] args) {
        Stack<String> stack = new Stack<>();
//        Scanner sc = new Scanner(System.in);
//        String str = sc.nextLine();

        for (String s : "(tasknumber[1])check".split("")) {
            if (s == "(" || s == "{" || s == "[") {
                stack.push(s);
                System.out.println(stack.head.item);
                continue;
            }
            if (s == ")" && stack.peek() == "(")
                stack.delete();
            if (s == ")" && stack.peek() != "(") {
                System.out.println("String is incorrect");
                break;
            }
            if ((s == "}") && (stack.peek() == "{"))
                stack.delete();
            if ((s == "}") && (stack.peek() != "{")) {
                System.out.println("String is incorrect");
                break;
            }
            if ((s == "]") && (stack.peek() == "["))
                stack.delete();
            if ((s == "]") && (stack.peek() != "[")) {
                System.out.println("Stack is incorrect");
                break;
            }
        }
    }
}