package AdditionalPoints;
// Может быть несколько реквестов от одного имени
// Программа выводит несколько вариантов(от 2х до 5ти), при этом указывая, кто может в то время

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class MeetingStage2 {
    public static void main(String[] args) {
        System.out.println("Stage II");
        int countOfMembers = 0;
        boolean nextMember = true;
        Scanner sc = new Scanner(System.in);
        List<Member2> listOfMember = new ArrayList<>(); // Array of members
String name;
        while (nextMember){
            System.out.println("Write name of member [" + countOfMembers + "]: ");
            name = sc.nextLine();
            if (name.equals("") || name.equals("\n"))
                name = sc.nextLine();
            System.out.println("Write count of requests: ");
            int req = sc.nextInt();
            listOfMember.add(new Member2(name, req));

            countOfMembers++;
            if (countOfMembers > 1){
                System.out.println("New member? (Yes - true, no - false)");
                nextMember = sc.nextBoolean(); }
            else{ nextMember = true;
            }
        }

        Member2[] temp = new Member2[listOfMember.size()];
        for (int i = 0; i < listOfMember.size(); i++){
            temp[i] = listOfMember.get(i);
        }



        Meeting2 meeting = new Meeting2(temp);
        meeting.Calculation1(meeting.requests, meeting.names);
        System.out.println("====================================================");
        meeting.Calculation2(temp);



    }
}

class Member2 {

    int[] time;   // Массив, который лежит в list,
    int countOfRequests;
    String name;

    public Member2(String name, int countOfRequests) {
        this.name = name;
        this.countOfRequests = countOfRequests;
        this.time = new int[countOfRequests * 2];
        setTime();
    }

    public int[] getTime() { return time;}
    public String getName() { return name;}

// Метод, определяющий местный массив time c парами hours
    public void setTime() {
        Scanner sc = new Scanner(System.in);
        // Защита и добавление в массив list массива из 2*countOfRequests элементов time
        boolean timefalse = true;
        for (int i = 0; i < countOfRequests; i++) { // На каждой паре элементов массива помещается request
            timefalse = true;
            while (timefalse) {
                {
                    System.out.println("Write time" + (2*i) + " & time" + (2*i+1) + " for member " + this.name + ": ");
                    int time1 = sc.nextInt();
                    int time2 = sc.nextInt();
                    if ((time1 >= 0)  && (time2 > time1) && (time2 <= 24)) {
                        this.time[2 * i] = time1;
                        this.time[2 * i + 1] = time2;
                        timefalse = false;
                        System.out.println("Time is correct, next step.");
                    } else {
                        System.out.println("Incorrect time. Again: ");
                    }
                }
            }
        }
    } // Теперь есть массив time с местными заявками
}

class Meeting2{
    public Member2[] members; // Массив из Member, который есть в constructor
    public int[][] requests;
    public String[] names;

    public Meeting2(Member2[] members){
        System.out.println("Creating meeing..");
        this.members = members;
        names = new String[members.length]; // ОБЪЯВЛЯЮ! массив для имён
        this.requests = new int[members.length][];
        fieldToArrays();
    }
    public void fieldToArrays(){
        for (int i = 0; i < members.length; i++){
            names[i] = members[i].name;
            requests[i] = members[i].time;
        }
    }   // Теперь у нас один большой класс Meeting, в котором есть массив с именами names и реквестами requests

    public void Calculation1(int[][] requests, String[] names){
        // Сначала надо вывести вариант, наиболее подходящий всем:
        int[] trueTime = new int[24];
        for (int i = 0; i < requests.length; i++){ // Прохожу по всем реквествам
            for (int j = 0; j < requests[i].length; j +=2) { // по реквесту [i], получаю ЧАСЫ
                for (int n = 0; n < trueTime.length; n++){
                    if(n >= requests[i][j] && n <= requests[i][j+1]){
                        trueTime[n]++;
                    }
                }
                trueTime[requests[i][j]]++;
            }
        }
        System.out.println("Могут все в " + IndexOfMax(trueTime));

    }
    public void Calculation2(Member2[] members) {
        boolean[] whoCan = new boolean[members.length];
        for (int i = 0; i < members.length - 1; i++) {
            if (members[i].time[1] >= members[i + 1].time[0]) {
                whoCan[i] = true;
                whoCan[i + 1] = true;
                System.out.println(members[i].name + " & " + members[i + 1].name + " can in " + members[i+1].time[0]);
            }
            if (members[i].time[0] >= members[i + 1].time[1]) {
                System.out.println(members[i].name + " & " + members[i + 1].name + " can in " + members[i].time[0]);
            }
        }
    }

    public int IndexOfMax(int[] array){
        int max = -1;
        int indexOfMax = -1;
        for (int i = 0; i < array.length; i++){
            if (array[i] > max) {
                max = array[i];
                indexOfMax = i;
            }
        }
        return indexOfMax;
    }
}
