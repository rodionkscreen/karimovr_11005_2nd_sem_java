//package AdditionalPoints;
//// Может быть несколько реквестов от одного имени
//// Программа выводит несколько вариантов(от 2х до 5ти), при этом указывая, кто может в то время
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Scanner;
//
//public class MeetingStage2 {
//    public static void main(String[] args) {
//        System.out.println("Stage II");
//        int countOfMembers = 0;
//        boolean nextMember = true;
//        Scanner sc = new Scanner(System.in);
//        List<Member2> listOfMember = new ArrayList<>(); // Array of members
//
//        while (nextMember){
//            System.out.println("Write name of member [" + countOfMembers + "]: ");
//            String name = sc.nextLine();
//            System.out.println("Write count of requests: ");
//            int req = sc.nextInt();
//            listOfMember.add(new Member2(name, req));
//
//            countOfMembers++;
//            if (countOfMembers > 1){
//                System.out.println("New member? (Yes - true, no - false)");
//                nextMember = sc.nextBoolean(); }
//            else{ nextMember = true;
//            }
//        }
//
////        listOfMember.toArray(); // лист Member2 преобразовал к массиву, чтобы потом было удобнее работать
////        Member2[] temp = (Member2[]) listOfMember.toArray();
//        Member2[] temp = new Member2[listOfMember.size()];
//        for (int i = 0; i < listOfMember.size(); i++){
//            temp[i] = listOfMember.get(i);
//        }
//
//
//        Meeting2 meeting = new Meeting2(temp);
//        meeting.Calculation1(meeting.requests, meeting.names);
//        System.out.println("====================================================");
//
//
//
//    }
//}
//
//class Member2 {
//
//    int[] time;   // Массив, который лежит в list,
//    int countOfRequests;
//    String name;
//
//    public Member2(String name, int countOfRequests) {
//        this.name = name;
//        this.countOfRequests = countOfRequests;
//        this.time = new int[countOfRequests * 2];
//        setTime();
//    }
//
//    public int[] getTime() { return time;}
//    public String getName() { return name;}
//// Метод, определяющий местный массив time c парами hours
//    public void setTime() {
//        Scanner sc = new Scanner(System.in);
//        // Защита и добавление в массив list массива из 2*countOfRequests элементов time
//        boolean timefalse = true;
//        for (int i = 0; i < countOfRequests; i++) { // На каждой паре элементов массива помещается request
//            timefalse = true;
//            while (timefalse) {
//                {
//                    System.out.println("Write time1 & time2 for member " + this.name + ": ");
//                    int time1 = sc.nextInt();
//                    int time2 = sc.nextInt();
//                    if ((time1 >= 0)  && (time2 > time1) && (time2 <= 24)) {
//                        this.time[2 * i] = time1;
//                        this.time[2 * i + 1] = time2;
//                        timefalse = false;
//                    } else {
//                        System.out.println("Incorrect time. Again: ");
//                    }
//                }
//            }
//        }
//    } // Теперь есть массив time с местными заявками
//}
//
//class Meeting2{
//    public Member2[] members; // Массив из Member, который есть в constructor
//    public int[][] requests;
//    public String[] names;
//
//    public Meeting2(Member2[] members){
//        System.out.println("Creating meeing..");
//        this.members = members;
//        names = new String[members.length]; // ОБЪЯВЛЯЮ! массив для имён
////        requests = new int[members.length][6]; Кажется это не обязательно, это ведь ссылочный тип..
//        fieldToArrays();
//    }
//    public void fieldToArrays(){
////        System.out.println("FieldToArrays");
//        for (int i = 0; i < members.length; i++){
//            this.names[i] = members[i].name;
//            System.out.println("Time:");
//            this.requests[i] = members[i].time; // !
//            System.out.println("Aftertime");
//        }
//        System.out.println("End FieldToArrays method");
//    }   // Теперь у нас один большой класс Meeting, в котором есть массив с именами и реквестами
//    public void Calculation1(int[][] requests, String[] names){
//        // Сначала надо вывести вариант, наиболее подходящий всем:
//        int[] trueTime = new int[24];
//        for (int i = 0; i < requests.length; i++){ // Прохожу по всем реквествам
//            for (int j = 0; j < requests[i].length; j +=2) { // по реквесту [i], получаю ЧАСЫ
//                for (int n = 0; n < trueTime.length; n++){
//                    if(n >= requests[i][j] && n <= requests[i][j+1]){
//                        trueTime[n]++;
//                    }
//                }
//                trueTime[requests[i][j]]++;
//            }
//        }
//        System.out.println("Могут все в " + IndexOfMax(trueTime));
//
//
//
//    }
//    public int IndexOfMax(int[] array){
//        int max = -1;
//        int indexOfMax = -1;
//        for (int i = 0; i < array.length; i++){
//            if (array[i] > max) {
//                max = array[i];
//                indexOfMax = i;
//            }
//        }
//        return indexOfMax;
//    }
//}
