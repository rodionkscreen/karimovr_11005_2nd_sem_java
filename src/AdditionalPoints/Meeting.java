package AdditionalPoints;
import java.util.*;

public class Meeting {
    private static int[] timeForMeeting = new int[24]; // Массив для подсчёта подходящего времени, используется в TimeForMeeting

    private List<Member> members;

    public Meeting(List<Member> listOfMembers) {
        members = listOfMembers;
    }

    private void TimeForMeeting(){
        for (int i = 0; i < members.size(); i++){
            int[] temp = (int[]) members.get(i).time; // temp - массив из 2х элементов со временем
            for (int j = temp[0]; j < temp[1]; j++){
                timeForMeeting[j]++;
            }
        }
    }
    public int Calculation(){
        int time = -1;
        int a = 0; // Для проверки значения (не номер)
        for (int i = 0; i < timeForMeeting.length; i++){
            if (timeForMeeting[i] > a){
                time = i;
                a = timeForMeeting[i];
            }
        }
        return time;
    }
}

class Member {
    int[] time = new int[2];    // Массив, который лежит в list,
    String name;
    public Member(String name) {
        this.name = name;
        setTime();
    }
    public void setTime() {
        Scanner sc = new Scanner(System.in);
        // Защита и добавление в массив list массива из 2х элементов time
        boolean timefalse = true;
        while (timefalse) {
            System.out.println("Write time1 & time2 for member " + this.name + ": ");
            int time1 = sc.nextInt();
            int time2 = sc.nextInt();
            if ((time1 >= 0) && (time1 <= 23) && (time2 > time1) && (time2 >= 0) && (time2 <= 24)) {
                this.time[0] = time1;
                this.time[1] = time2;
                timefalse = false;
            } else {
                System.out.println("Incorrect time. Again: ");
            }
        }
    }
}

class Main{
    public static void main(String[] args) {
        System.out.println("Stage I");
        int countOfMembers = 0;
        boolean nextMember = true;
        Scanner sc = new Scanner(System.in);
        List<Member> listOfMember = new ArrayList<>(); // Array of members

        while (nextMember){
            System.out.println("Write name of member [" + countOfMembers + "]: ");
            String name = sc.nextLine();
            listOfMember.add(new Member(name));
//          listOfMember.get(countOfMembers);

            countOfMembers++;
            if (countOfMembers > 1){
            System.out.println("New member? (Yes - true, no - false)");
            nextMember = sc.nextBoolean(); }
            else{ nextMember = true;
            }
        }
        listOfMember.toArray();
        Meeting meeting = new Meeting(listOfMember);
        int time = meeting.Calculation();
        System.out.println("====================================================");
        System.out.println("Time for meeting: " + time);


    }
}