package ControlWork25_03;

import kotlin.jvm.internal.SpreadBuilder;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Tasks {
    private SpreadBuilder buyers;

    public static void main(String[] args) {
        ArrayList<Buyer> buyers = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader("D:\\Study\\JAVA\\2nd sem\\karimovr_11005_2nd_sem_java\\src\\ControlWork25_03\\orders.txt"));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] strings = line.split("\\|");

//                Buyer buyer = getBuyer(strings[0]);
                for (int i = 0; i < buyers.size(); i++){
                    if (buyers.get(i).getName().equals(strings[0])) {
                        Buyer buyer = buyers.get(i);
                    }
                    else{
                        buyers.add(new Buyer(strings[0]));
                        Buyer buyer = buyers.get(i);
                    }
                }
//                buyer.addOrder(strings[1], strings[2], Integer.parseInt(strings[3]));



            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

//    Buyer getBuyer(String name){
//        for (int i = 0; i < buyers.size(); i++){
//            if (buyers.get(i).getName().equals(name)){
//                return buyers.get(i);
//            }
//        }
//
//
//    }

//    static class Buyer implements {
    static class Buyer  {
        String name;
        ArrayList<City> cities = new ArrayList<>();

        public Buyer(String name) {
            this.name = name;
        }

        public int getCitiesSize() {
            return cities.size();
        }

        void addOrder(String city, String name, int count) {
            for (City c : cities) {
                if (c.getName().equals(city)) {
//                    c.addOrder(name, count);
                    return;
                }
            }
            cities.add(new City(city));
            addOrder(city, name, count);
        }
        public String getName(){
            return name;
        }
        public int getCitiesCount(){
            return cities.size();
        }
        public int getOrdersCount(){
            int count = 0;
            for (City c : cities){
                count += c.getOrderCount();
            }
            return count;
        }
        public String toString(){
            return "" + name + " " + cities;
        }

        class City {
            String name;
            ArrayList<Order> orders = new ArrayList<>();

            public City(String name) {
                this.name = name;
            }

//            void addOrder(String name, int count) {
//                orders.add(new Order(name, count));
//            }

            public String getName() {
                return name;
            }

            public int getOrderCount() {
                return orders.size();
            }

            public String toString() {
                return "" + name + " " + orders;
            }

        }
    }
//    static class BuyerByNameComparator implements Comparator<>

    class Order {
        String name;
        int count;

        public Order(String name, int count) {
            this.name = name;
            this.count = count;
        }

        public String toString() {
            return "" + name + " " + count;
        }
    }

}