//package class_work.cw22_02;
//import javax.xml.soap.Node;
//import java.util.*;
//
//class LinkedList< > implements Iterable<T>{
//    private static class Node<T> {
//        T item;
//        Node next;
//    }
//    private Node<T> root;
//    public void add (T item){}
//
//    public T get(int id) {}
//
//    public int size() {}
//    }
//}
//
//class Main {
//    ////    public static <T> void genericPrint(T value) {
//    ////        System.out.println(value);
//
//    ////    public static <T> ArrayList<T> asList(T[] items) {
//    public static <T> ArrayList<T> asList(T ... items) {
//        ArrayList<T> list = new ArrayList<>();
//        for (T item : items) {
//            list.add(item);
//        }
//
//    return list;
//}
//public static <T> void printAll (T ... items){
//    for (T item : items)
//        System.out.println(item);
//}
//    public static void main(String[] args) {
//        ArrayList<Integer> ints = Main.<Integer>asList(new Integer[]{18, 20, 30});
//        printAll("String", 1, 2, 'c');
//        ArrayList list = asList("string", 10, 20);
//        for (int i = 0; i < list.size(); i++)
//            System.out.println(list.get(i));
//    }
//}
//
//
//
//
//
//public class Main {
//    public static <T> ArrayList<T> asList(T... items){
//        ArrayList<T> list = new ArrayList<>();
//        for (T item : items) {
//            list.add(item);
//        }
//
//        return list;
//    }
//
//    public static <T> void printAll(T... items) {
//        for (T item : items)
//            System.out.println(item);
//    }
//
//    public static void main(String[] args) {
//        ArrayList<String>[] list = new ArrayList[1];
//        ArrayList<Integer> ints = new ArrayList<>();
//        ints.add(10);
//
//        Object[] objects = list;
//        objects[0] = ints;
//
//        System.out.println(list[0].get(0));
//    }
//}