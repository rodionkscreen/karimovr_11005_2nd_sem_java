package class_work.cw08_04;

import java.lang.reflect.Method;

// Метаданные - данные о данных. Доп инфа, как работать с к-л данными. Например картинка: в каком формате записаны пиксель, размер пикселя, плотность, формат картинки и пр
// Аннотации - пометки, говорящие компилятору как работать с определёнными данными (напр. Override).
// Встроенные типы аннотаций: @SuppressWarnings() - игнорирование предупреждений. @Deprecated - метод устаревший и более не используется, @Override
//
@Annotation
public class work {

    @Deprecated
    public static  void test(){}


    @UseCase(id = 1, description = "Main")
    public static void main(String[] args) {
        test(); // В подсказке не советует использовать
    }

//  @UseCase(id = 1, description = "Main") // можно применять к полю. Но после того, как написали @Target(ElementType.METHOD), стало нельзя
    public int intValue;


    public static void main2(String[] args) {
        try {
            Class targetClass = Class.forName("Password");
            for (Method m : targetClass.getDeclaredMethods()){
                UseCase useCase = m.getAnnotation(UseCase.class);
                if (useCase == null)
                    continue;
                System.out.println("Found annotation" + useCase.id() + useCase.description());
            }
        } catch (ClassNotFoundException e){
            e.printStackTrace();
        }
    }
}

class PasswordsUtils{
    @UseCase(id = 1, description = "Password must contains at least one number")
    public static boolean validate(String password){
        return password.matches("\\w*\\d\\w*"); // буквы, потом одно число, потом опять сколько-то букв
    }

    @UseCase(id = 2, description = "No description")
    public static String encryptPassword(String password){
        return new StringBuilder(password).reverse().toString();
    }
}