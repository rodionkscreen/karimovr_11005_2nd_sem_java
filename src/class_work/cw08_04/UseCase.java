package class_work.cw08_04;
// описывает сценарий, для реализации

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD) // теперь при добавлении аннотации, кроме методов, будет ошибка
//@Retention(RetentionPolicy.CLASS) // В какой момент доступна // Доступна при подключении новых классов. (используем, когда подключаем классы из других пакетов, те import добавляем в runtime)
//@Retention(RetentionPolicy.SOURCE) //
@Retention(RetentionPolicy.RUNTIME) // Когда эту аннотацию можно увидеть
public @interface UseCase {
    public int id();
    public String description();
}
