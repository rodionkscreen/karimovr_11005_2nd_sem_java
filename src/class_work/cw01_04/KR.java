package class_work.cw01_04;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

class Order{
    String name;
    Integer count;
}

class City{
    String name;
    Order order;
}

class Buyer{
    String name;
    City city;
}

public class KR {
    public static void main(String[] args) throws FileNotFoundException {
        Map<String, HashMap<String, HashMap<String, Integer>>> buyersMap = new HashMap<>();
        Scanner in = new Scanner(new FileReader("order.txt"));

        while (in.hasNextLine()){
            String[] line = in.nextLine().split("\\|");
            if(!buyersMap.containsKey(line[0]))
            {
                HashMap<String, Integer> order = new HashMap<>();
                order.put(line[2], Integer.parseInt(line[3]));

                HashMap<String, HashMap<String, Integer>> city = new HashMap<>();
                city.put(line[1], order);
                buyersMap.put(line[0], city);
                continue;
            }

            HashMap<String, HashMap<String, Integer>> city = new HashMap<>();

            if (!city.containsKey(line[1])){
                HashMap<String, Integer> order = new HashMap<>();
                order.put(line[2], Integer.parseInt(line[3]));
                city.put(line[1], order);
                continue;
            }

            HashMap<String, Integer> orders = city.get(line[1]);

            if (!orders.containsKey(line[2])){
                orders.put(line[2], Integer.parseInt(line[3]));
            }

        }

String itemN = null; String cityN = null; String nameN = null; Integer count = null;

        for (Map.Entry<String, HashMap<String, HashMap<String, Integer>>> buyer : buyersMap.entrySet()){
            if (nameN != buyer.getKey()){
                nameN = buyer.getKey();
                System.out.println(nameN + ":");
            }
            HashMap<String, HashMap<String, Integer>> cityMap = buyer.getValue();

            for (Map.Entry<String, HashMap<String, Integer>> city : cityMap.entrySet()){
                if (cityN != city.getKey()){
                    cityN = city.getKey();
                    System.out.println("    " + cityN + ":");
                }

                HashMap<String, Integer> orderMap = city.getValue();

                for (Map.Entry<String, Integer> order : orderMap.entrySet()){
//                    if (itemN != order.getKey())
//                    itemN = order.getKey();
//                    System.out.println(itemN);
                    System.out.println("        " + order.getKey() + " - " + order.getValue());
                }
            }



        }
    }

//    public static ArrayList<City> getCities(HashMap<String, HashMap<String, Integer>> citiesMap){
//        for (Map.Entry<String, HashMap<String, Integer>> cityEntry : citiesMap.entrySet()){
//            City city = new City();
//            city.name = cityEntry.getKey();
//            city.orders = getOrders(cityEntry.getValue());
//            cities.add(city);
//        }
//
//        return cities;
//    }
//
//    public static ArrayList<Order> getOrders (HashMap<String, Integer> ordersMap){
//        ArrayList<Order> orders = new ArrayList<>();
//
//        for (Map.Entry<String, HashMap<String, Integer>> cityEntry : citiesMap.entrySet()){
//
//        }
//    }
}


