package class_work.cw19_05;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Stream;

import static java.lang.Thread.sleep;
// классная работа 1 часть
//public class Cw {
//    public static void main(String[] args) {
//        String[] a = {"1", "2", "44"};
//        System.out.println(new Cw().countSymbol(a));
//
//
//    }
//
//    public String countSymbol(String[] A){
//        var t = Arrays.stream(A).reduce((a, b) -> String.valueOf(a.length() + b.length())).toString();
//        return t;
//    }
//}

// Threads

class Thread1 implements Runnable{

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName());
        System.out.println("Hello, Senex is high  tab always");
        try {
            sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        for (int i = 0; i < N; i++){
            new Thread(new Thread1()).start();
        }
    }
}