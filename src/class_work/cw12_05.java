package class_work;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class cw12_05 {


    public static void task2(String input, int n){
//        Arrays.stream(input.split(" "))
//                .collect(Collectors.groupingBy(x -> x, Collectors.counting()))
//                .forEach((key, value) - > System.out.println(key + " - " + value));
    }

    public static List<Integer> task3(Integer[] a, Integer[] b, int k1, int k2){
        return Stream.concat(
                Arrays.stream(a).filter(x -> x < k1),
                Arrays.stream(b).filter(x -> x < k2)
        ).toList();
    }

    public static List<Integer> task4(Integer[] a){ // из всех, заканчивающихся на одну и ту же цифру, получить максимальное: 123, 536, 253, 236 -> 536, 253
        return Arrays.stream(a)
                .collect(Collectors.groupingBy(x -> x % 10))
                .values()
                .stream()
                .map(x -> x.stream().max(Integer::compare))
                .filter(x -> x.isPresent())
                .map(x -> x.get())
                .toList();
        // Для провеки вместо return в начале пишем var t =
    }

    public static Map<Integer, Integer> task5(Integer[] a){ // получить мапу, где ключом будет последняя цифра, а ключом - все числа, заканчивающиеся на эту цифру: 123, 546, 243, 256 -> 3 - (123, 243), 6 - (546, 256)
//        var t1=  Arrays.stream(a)
//                .collect(Collectors.groupingBy(x -> x % 10))
//                .entrySet().stream()
//                .collect(Collectors.toMap(x -> x.getKey(),
//                        x -> x.getValue().stream().reduce(0, Integer::sum)));
        return Arrays.stream(a)
                .collect(Collectors.groupingBy(x -> x % 10))
                .entrySet().stream()
                .collect(Collectors.toMap(x -> x.getKey(), x -> x.getValue().stream().reduce(0, Integer::sum)));
    }


    //13_05
    class Product{
        String name;

    }
    class Purchase{
        String[] product;
        String buyer;
        int count;
        String shop;
        String productName;

        public Purchase(String[] product, String buyer, int count, String shop, String productName) {
            this.product = product;
            this.buyer = buyer;
            this.count = count;
            this.shop = shop;
            this.productName = productName;
        }
    }
    public static void main(String[] args) {
        Purchase[] purchases = new Purchase[3];
        var t = Arrays.stream(purchases)
                .collect(Collectors.groupingBy(
                        x -> x.buyer,
                        Collectors.summingInt(p -> p.count)
                ));
        var t1 = Arrays.stream(purchases)
                .collect(Collectors.groupingBy(
                        x -> x.buyer,
                        Collectors.reducing(
                                0,
                                p -> p.count,
                                (a,b) -> a + b
                        )
                ));



    }
}
