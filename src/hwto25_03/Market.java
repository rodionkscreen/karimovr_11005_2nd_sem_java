package hwto25_03;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

//Система хранения товаров на складе.
//1. У каждого товара хранится его название и количество. +
//2. Список (или массив) товаров должны храниться в файле. +
//3. Реализовать операции:
//-Вывод всех товаров; +
//-Добавление нового в список; +
//-Удаление товара со склада; +
//-Изменение количества товара. +


public class Market {
    public HashMap<String, Integer> mapProducts;
    final String path ="src/hwto25_03/MarketData.txt";

    public Market(){
        this.mapProducts = new HashMap<String, Integer>();
    }

    public void addProduct(String name, int count){
        if (mapProducts.containsKey(name)){
            int temp = mapProducts.get(name) + count;
            mapProducts.put(name, temp);
        } else{
            mapProducts.put(name, count);
        }
        addToFile();
    }

    public void addToFile(){
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(path))) {

            for (Map.Entry entry : mapProducts.entrySet()){
                String key = (String) entry.getKey();
                Integer value  = (Integer) entry.getValue();
                bufferedWriter.write(key + ": " + value + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void printProduct(){
//        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(path))){
//            while (bufferedReader.read() != -1){
//                System.out.println(bufferedReader.readLine());
//            }
//        } catch (IOException e){
//            System.out.println(e.getMessage());
//        }
        System.out.println(mapProducts);

    }

    public void changeProduct(String name, int count){
        if (mapProducts.containsKey(name)){
            count = mapProducts.get(name) + count;
            mapProducts.put(name, count);
        }else
            System.out.println("No such name");

        addToFile();
    }

    public void deleteProduct(String name){
        if (mapProducts.containsKey(name)){
            mapProducts.remove(name);
        }else
            System.out.println("No such name in data");

        addToFile();
    }
}

class Main {
    public static void main(String[] args) {
        Market market = new Market();
        market.addProduct("Milk", 2);
        market.addProduct("Bread", 3);
        market.addProduct("Chicken", 1);
        market.printProduct();

        market.addToFile();

//        market.deleteProduct("Chicken");
//        market.printProduct();

    }
}


