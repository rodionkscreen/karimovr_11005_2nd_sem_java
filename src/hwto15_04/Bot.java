//package hwto15_04;
//
//import java.util.*;
//import java.lang.reflect.*;
//
//
//public class Bot {
//    /* public String processUserInput(String input) {
//        if (input.isEmpty())
//            return "Я вас не понимаю";
//
//        String[] splitted = input.split(" ");
//        String command = splitted[0].toLowerCase();
//        String[] args = Arrays.copyOfRange(splitted, 1, splitted.length);
//
//        return "Вы ввели команду " + command;
//    } */
//    public String processUserInput(String input) {
//        if (input.isEmpty())
//            return "Я вас не понимаю";
//
//        String[] splitted = input.split(" ");
//        String command = splitted[0].toLowerCase();
//        String[] args = Arrays.copyOfRange(splitted, 1, splitted.length);
//
//        Method m = commands.get(command);
//
//        if (m == null)
//            return "Я вас не понимаю";
//
//        try {
//            return (String) m.invoke(this, (Object) args);
//        } catch (Exception e) {
//            return "Что-то пошло не так, попробуйте ещё раз";
//        }
//    }
//
//
//    @Annotations(aliases = {"привет", "hello"},
//            args = "",
//            description = "Будь культурным, поздоровайся.")
//    public String hello() {
//        return "Привет!!";
//    }
//
//    @Annotations(aliases = {"пока", "bye"},
//            args = "",
//            description = "")
//    public String bie() {
//        return "Возвращайся!";
//    }
//
//   /* @Annotations(aliases = {"помощь", "помоги", "команды", "help"},
//            args = "",
//            description = "Выводит список команд")
//    public String help() {
//        return "Я умею в 3 команды: привет, пока, помощь.";
//    } */
//   @Annotations(aliases = {"помощь", "команды", "help"},
//           args = "",
//           description = "Выводит список команд")
//   public String help(String[] args) {
//       if (args.length() == 1) {
//           StringBuilder builder = new StringBuilder("Я умею в такие команды:\n");
//
//           for (Method m : this.getClass().getDeclaredMethods()) {
//               if (!m.isAnnotationPresent(Annotations.class))
//                   continue;
//
//               Annotations cmd = m.getAnnotation(Annotations.class);
//               builder.append(Arrays.toString(cmd.aliases())).append(": ").append(cmd.description()).append(" - ").append(cmd.args()).append("\n");
//           }
//
//           return builder.toString();
//       } else{
//           StringBuilder builder = new StringBuilder();
//           for (Method m : this.getClass().getMethod(args[0])){
//               builder.append(m);
//           }
//           return builder.toString();
//
//       }
//   }
//
//    @Annotations(aliases = {"сумма", "sum"},
//            args = "",
//            description = "Сумма")
//   public int sum(String[] args){
//      int a = Integer.parseInt(args[0]);
//      int b = Integer.parseInt(args[1]);
//      return a + b;
//   }
//   @Annotations(aliases = {"произведение", "composition"},
//            args = "",
//            description = "Произведение")
//   public int composition(String[] args){
//      int a = Integer.parseInt(args[0]);
//      int b = Integer.parseInt(args[1]);
//      return a * b;
//   }
//   @Annotations(aliases = {"квадрат", "quad"},
//            args = "",
//            description = "Квадрат")
//   public int quad(String[] args){
//      int a = Integer.parseInt(args[0]);
//      return a * a;
//   }
//
//
//    private HashMap<String, Method> commands;
//
//    public Bot() {
//        commands = new HashMap<>();
//        for (Method m : this.getClass().getDeclaredMethods()) {
//            if (!m.isAnnotationPresent(Annotations.class))
//                continue;
//            Annotations cmd = m.getAnnotation(Annotations.class);
//            for (String name : cmd.aliases())
//                commands.put(name, m);
//        }
//    }
//}
//
//
//class Main {
//    public static void main(String[] args) {
//        Scanner in = new Scanner(System.in);
//        Bot bot = new Bot();
//
//        while (true) {
//            String message = in.nextLine();
//            if (message.equals("exit"))
//                break;
//
//            String answer = bot.processUserInput(message);
//            System.out.println(answer);
//        }
//    }
//}
//
