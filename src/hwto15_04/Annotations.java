package hwto15_04;

import java.lang.annotation.*;


@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Annotations {
    String[] aliases(); // Возможные названия команды
    String args(); // описание аргументов команды
    String description(); // описание самой команды
}


