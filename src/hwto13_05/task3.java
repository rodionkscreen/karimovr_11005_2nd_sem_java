package hwto13_05;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class task3 {
    public static void main(String[] args) {
        String path = "src/hwto13_05/data.txt";
        Map<String, Integer> map = new HashMap<>();
        try {
            map = Files.lines(Paths.get(path))
                    .collect(Collectors.toMap(
                            o1 -> o1.substring(0, o1.indexOf('|')),
                            o1 -> Integer.parseInt(o1.substring(o1.indexOf('|') + 1)),
                            (integer, integer2) -> integer += integer2));
        }catch(IOException e){
            e.printStackTrace();
        }
        System.out.println(map);
    }
}
