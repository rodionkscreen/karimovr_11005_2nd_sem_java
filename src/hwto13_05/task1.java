package hwto13_05;

import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Stream;

public class task1 {
    public static void main(String[] args) {
        String[] array = new String[] {"anulll", "one", "atwo", "bthree", "afour", "bfive"};
        Arrays.stream(array).filter(x -> x.matches("a\\w*"))
                .sorted(Comparator.comparingInt(String::length))
                .forEach(System.out::println);
    }
}
