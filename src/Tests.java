import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

class Test1{
    private static final String PATH = "src/text.txt";

    public static void main(String[] args) {
        try (BufferedReader reader = new BufferedReader(new FileReader(new File(PATH)))){
            String line;
            while ((line = reader.readLine()) != null){
                System.out.print(line + "/n");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        String[] text = {"One", "Two", "Three"};
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(new File(PATH)))){
            for (String value : text)
                writer.write(value + " ");
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedReader reader = new BufferedReader(new FileReader(new File(PATH)))){
            String line;
            while ((line = reader.readLine()) != null){
                System.out.print(line + "/n");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}