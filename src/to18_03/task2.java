package hwto18_03;

import java.util.HashMap;
import java.util.Map;

class taskto18_03 {
    public static void main(String[] args) {
        Map<String, Map<String, Integer>> map = new HashMap<>();
        String[] string = new String[6];
        string[0] = "Вася ручка 5";
        string[1] = "Вася карандаш 3";
        string[3] = "Петя книга 2";
        string[4] = "Петя книга 3";
        string[2] = "Вася книга 1";
        string[5] = "Вася ручка 4";
        for (String str : string) {
            String[] temp = str.split(" ");// "Вася, ручка, 5"
            // Если нет ключа по имени
            if (!map.containsKey(temp[0])) {
                Map<String, Integer> mapTemp = new HashMap<>();

                mapTemp.put(temp[1], Integer.parseInt(temp[2]));
                map.put(temp[0], mapTemp);
            } else { // Если нет второго ключа
                if (!map.get(temp[0]).containsKey(temp[1])) {
                    map.get(temp[0]).put(temp[1], Integer.parseInt(temp[2]));

                } else{ // Если есть второй ключ и надо увеличить значение
                    int value = map.get(temp[0]).get(temp[1]);
                    value += Integer.parseInt(temp[2]);
                    map.get(temp[0]).put(temp[1], value);
                }
            }
        }
        System.out.println(map);
    }
}
