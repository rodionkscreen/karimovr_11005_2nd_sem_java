package AdditionalPoints_sem2;


import java.util.*;

public class CRUDHolder<T extends Comparable<T> & Printable> implements Printable{
    ArrayList<T> items = new ArrayList<>();

    public void create(T item) throws IllegalArgumentException {
        if (!items.contains(item)) {
            items.add(item);
        } else {
            throw new IllegalArgumentException();
        }
    }

    public T read(int id) throws NoSuchElementException {
        if (items.size() >= id){
            return items.get(id);
        } else throw new NoSuchElementException();
    }

    public void update(T item) throws NoSuchElementException {
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).compareTo(item) == 0) {
                items.set(i, item);
                return;
            }
        }
        throw new NoSuchElementException();
    }

    public void delete(T item) throws NoSuchElementException {
        T temp;
        for (int i = 0; i < items.size(); i++){
            if (items.get(i).compareTo(item) == 0) {
                items.remove(i);
                break;
            }
        } throw new NoSuchElementException();
    }
//
//    public void printAll() {
//        items.forEach(Printable::print);
//    }

    @Override
    public void print() {
        items.forEach(Printable::print);
    }
    public int getCount(){
        return items.size();
    }
}

interface Printable {
    void print();
}