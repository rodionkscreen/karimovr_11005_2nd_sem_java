package AdditionalPoints_sem2;

public class ForTest implements Printable, Comparable<ForTest>{
    private final int value;
    private final String string;

    public ForTest(int value, String string) {
        this.value = value;
        this.string = string;
    }

    @Override
    public int compareTo(ForTest givenObject) {
        return Integer.compare(value, givenObject.getValue());
    }

    public int getValue() {
        return value;
    }

    public String getString() {
        return string;
    }

    @Override
    public void print() {
        System.out.println(toString());
    }

    @Override
    public String toString() {
        return "TestObject{" +
                "value=" + value +
                ", string='" + string + '\'' +
                '}';
    }
}
